﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using JetBrains.Annotations;
using Nevrolens.Helpers;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InfoBoard : MonoBehaviour
{
    
    // private string path = "whs_preview";

    private static NetworkBrainManager BrainManager => NetworkBrainManager.Instance;
    
    struct InfoBoardData
    {
        internal string title;
        [CanBeNull] internal BrainPart brainPart;
        internal string text;
        internal string resource;
        

    }

    [SerializeField] private TextAsset infoBoardTextSource;
    [SerializeField] private TextMeshProUGUI infoBoardDescriptionText;
    [SerializeField] private Image infoBoardDescriptionImage;

    private Dictionary<string, InfoBoardData> infoBoards;

    private InfoBoardData? defaultBoard = null;
    
    // Start is called before the first frame update
    void Start()
    {
        infoBoards = ParseInfoBoardText(infoBoardTextSource.text);
        FillInfoBoard();
    }

    public void OnSelectBrianPart() => FillInfoBoard();

    private void FillInfoBoard()
    {
        var bp = BrainManager.SelectedBrainPart;

        var data = defaultBoard.GetValueOrDefault();
        if (bp != null)
            infoBoards.TryGetValue(bp.name, out data);
        
        SetInfoBoard(data);
    }

    void SetInfoBoard(InfoBoardData data)
    {
        var title = data.title.Style("Title").Newline(2);
        infoBoardDescriptionText.text = title + data.text;
        if (!string.IsNullOrWhiteSpace(data.resource))
            infoBoardDescriptionImage.sprite = LoadNewSprite(data.resource, isResource: true); 
        else 
            infoBoardDescriptionImage.gameObject.SetActive(false);
    }

    Dictionary<string, InfoBoardData> ParseInfoBoardText(string infoBoardText)
    {

        var infoBoards = new Dictionary<string, InfoBoardData>();
        
        var lines = infoBoardText.Split(Environment.NewLine.ToCharArray());
        var currentInfoBoardName = string.Empty;
        var currentInfoBoardResource = string.Empty;
        var currentInfoBoardText = string.Empty;

        InfoBoardData CreateInfoBoardData()
        {
            var result = new InfoBoardData();
            
            result.text = currentInfoBoardText;

            var res = string.Empty;

            if (currentInfoBoardResource.Contains("."))
                res = currentInfoBoardResource.Split('.')[0];
            else
                res = currentInfoBoardResource;
            
            result.resource = res;

            if (!BrainManager.NamedBrainParts.TryGetValue(currentInfoBoardName, out var brainPart))
            {
                result.title = currentInfoBoardName;
                
                if (defaultBoard != null)
                    Debug.LogError("To many non brain part names in InfoBoard source");
                else
                    defaultBoard = result;
            }
            else
            {
                result.title = brainPart.DisplayName;
                result.brainPart = brainPart;
            }
            


            return result;
        }
        
        foreach (var line in lines)
        {
            
            if (string.IsNullOrWhiteSpace(line) || line[0] == '!')
                continue;
            if (line[0] == '@')
            {
                if (!string.IsNullOrEmpty(currentInfoBoardName))
                {
                    infoBoards[currentInfoBoardName] = CreateInfoBoardData();
                    
                    currentInfoBoardName = string.Empty;
                    currentInfoBoardResource = string.Empty;
                    currentInfoBoardText = string.Empty;
                }

                currentInfoBoardName = line.Substring(1).Trim();
            }
            else if (line[0] == '+')
            {
                currentInfoBoardResource = line.Substring(1).Trim();
            }
            else
            {
                var textLine = line + Environment.NewLine;
                currentInfoBoardText += textLine;

            }
            
        }
        
        if (!string.IsNullOrEmpty(currentInfoBoardName))
        {
            infoBoards[currentInfoBoardName] = CreateInfoBoardData();
        }

        return infoBoards;

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public Sprite LoadNewSprite(string filePath, float pixelsPerUnit = 100.0f, bool isResource = false) {

     // Load a PNG or JPG image from disk to a Texture2D, assign this texture to a new sprite and return its reference
     var spriteTexture = isResource ? LoadTextureFromResources(filePath) : LoadTexture(filePath);
     var newSprite = Sprite.Create(spriteTexture, new Rect(0, 0, spriteTexture.width, spriteTexture.height),new Vector2(0,0), pixelsPerUnit);

     return newSprite;
    }

    public static Texture2D LoadTextureFromResources(string resourceName) => Resources.Load<Texture2D>(resourceName);

    public static Texture2D LoadTexture(string filePath) {
   
       // Load a PNG or JPG file from disk to a Texture2D
       // Returns null if load fails

       if (File.Exists(filePath)){
         var fileData = File.ReadAllBytes(filePath);
         var tex2D = new Texture2D(2, 2);
         if (tex2D.LoadImage(fileData))           // Load the imagedata into the texture (size is set automatically)
           return tex2D;                 // If data = readable -> return texture
       }  
       Debug.LogWarning("No sprite");
       return null;                     // Return null if load failed
     }
}
