using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.UI;
using Microsoft.MixedReality.Toolkit.Utilities;
using Photon.Pun;
using UnityEngine;

public class SnapToAxisConstrain : TransformConstraint, IMixedRealityPointerHandler
{
    [SerializeField]
    [EnumFlags]
    [Tooltip("Constrain movement along an axis")]
    private AxisFlags constraintOnMovement = (AxisFlags) 0b11;

    /// <summary>
    /// Constrain movement along an axis
    /// </summary>
    public AxisFlags ConstraintOnMovement
    {
        get => constraintOnMovement;
        set => constraintOnMovement = value;
    }

    public override TransformFlags ConstraintType => TransformFlags.Move;
	
    [Tooltip("The constrained movement magnitude to trigger unsnapping")]
    [SerializeField] private float unsnapTreshold = 0.15f;
	
    [Tooltip("The distance to an axis that will activate snapping")]
    [SerializeField] private float snapTreshold = 0.02f;

    [SerializeField] private AudioClip snapAudioClip;
    [SerializeField] private AudioClip unsnapAudioClip;
	
    private Vector3 constrainedMovement;

    private AudioSource snapSource;

    private bool isActive = false;
    private bool IsActive
    {
        get => isActive;
        set
        {
            if (value)
            {
                ConstraintOnMovement = 0; // Set to unsnapped;
            }
            
            TryShouldSnap(!value, !value); // Try to snap to an axis;

            isActive = value;

        }
    }

    private void Start()
    {
        snapSource = GetComponent<AudioSource>();
        // if (PhotonNetwork.IsConnected)
        //     IsActive = PhotonView.Get(this).IsMine;
    }

    // new void OnEnable()
    // {
    //     base.OnEnable();
    //     if (PhotonNetwork.IsConnected && TryGetComponent<NetworkOwnedObject>(out var obj))
    //         obj.TransferOwnershipEvent?.AddListener(OnNetworkTransferOwnership);
    // }
    //
    // new void OnDisable()
    // {
    //     base.OnDisable();
    //     if (PhotonNetwork.IsConnected && TryGetComponent<NetworkOwnedObject>(out var obj))
    //         obj.TransferOwnershipEvent?.RemoveListener(OnNetworkTransferOwnership);
    // }

    // void OnNetworkTransferOwnership(bool isMine)
    // {
    //     Debug.Log($"Ball is mine {isMine}");
    //     IsActive = isMine;
    // }

    private bool IsSnapped => constraintOnMovement != 0;

    private bool ShouldUnsnap()
    {
        return constrainedMovement.magnitude > unsnapTreshold;
    }

    private bool ShouldSnap(out AxisFlags axisFlags)
    {
        var pos = transform.localPosition;
        axisFlags = 0;
		
        // Snap to Z-axis
        if (Mathf.Abs(pos.x) < snapTreshold && Mathf.Abs(pos.y) < snapTreshold)
            axisFlags = AxisFlags.XAxis | AxisFlags.YAxis;
		
        // Snap to Y-axis
        if (Mathf.Abs(pos.x) < snapTreshold && Mathf.Abs(pos.z) < snapTreshold)
            axisFlags = AxisFlags.XAxis | AxisFlags.ZAxis;

        // Snap to X-axis
        if (Mathf.Abs(pos.y) < snapTreshold && Mathf.Abs(pos.z) < snapTreshold)
            axisFlags = AxisFlags.YAxis | AxisFlags.ZAxis;

        return axisFlags != 0;
    }

    private void Snap()
    {
        var local = gameObject.transform.localPosition;
        if (constraintOnMovement.HasFlag(AxisFlags.XAxis))
            local.x = 0;
        if (constraintOnMovement.HasFlag(AxisFlags.YAxis))
            local.y = 0;
        if (constraintOnMovement.HasFlag(AxisFlags.ZAxis)) 
            local.z = 0;
          
        gameObject.transform.localPosition = local;
        
    }

    private void Update()
    {

        if (!IsActive) return;
        if (IsSnapped)
            Snap();
        
    }

    public void OnPointerDragged(MixedRealityPointerEventData eventData)
    {
        var toPointer = eventData.Pointer.Position - gameObject.transform.position; 
        
        if (IsSnapped && ShouldUnsnap(toPointer))
        {
            constraintOnMovement = 0;
			   
            if (snapSource && unsnapAudioClip)
            {
                snapSource.clip = unsnapAudioClip;
                snapSource.Play();
            }
        }
    }

    private bool ShouldUnsnap(Vector3 draggingVector)
    {
        var draggingCoef = Vector3.Dot(constrainedMovement, draggingVector); // This should somewhat represents the amount of dragging aplied just by being faraway and having to pull more.
        return constrainedMovement.magnitude - draggingCoef > unsnapTreshold;
    }

    void TryShouldSnap(bool doSnap = false, bool playSound = true)
    {
        if (!IsSnapped && ShouldSnap(out var axisFlags))
        {
            constraintOnMovement = axisFlags;
            if (doSnap)
                Snap();
			  
            if (playSound && snapSource && snapAudioClip)
            {
                snapSource.clip = snapAudioClip;
                snapSource.Play();
            }
        }
    }

    public void OnPointerUp(MixedRealityPointerEventData eventData)
    {
        // TryShouldSnap(); 
        IsActive = false;
    }

    public void OnPointerDown(MixedRealityPointerEventData eventData)
    {
        IsActive = true;
    }
    
    
    // This function is copied from MoveAxisConstrait.cs however is not responsible for contraining, its purpose is to populate (Vector3) constrainedMovement.
    // I have wasted some time trying to do the same in Update loop with local axes...
    public override void ApplyConstraint(ref MixedRealityTransform transform)
    {
        constrainedMovement = Vector3.zero;
        Quaternion inverseRotation = Quaternion.Inverse(worldPoseOnManipulationStart.Rotation);
        Vector3 position = transform.Position;
        if (constraintOnMovement.HasFlag(AxisFlags.XAxis))
        {
            constrainedMovement.x = position.x;
            position = inverseRotation * position;
            position.x = (inverseRotation * worldPoseOnManipulationStart.Position).x;
            position = worldPoseOnManipulationStart.Rotation * position;
            constrainedMovement.x -= position.x;
        }
        if (constraintOnMovement.HasFlag(AxisFlags.YAxis))
        {
            constrainedMovement.y = position.y;
            position = inverseRotation * position;
            position.y = (inverseRotation * worldPoseOnManipulationStart.Position).y;
            position = worldPoseOnManipulationStart.Rotation * position;
            constrainedMovement.y -= position.y;
        }
        if (constraintOnMovement.HasFlag(AxisFlags.ZAxis))
        {
            constrainedMovement.z = position.z;
            position = inverseRotation * position;
            position.z = (inverseRotation * worldPoseOnManipulationStart.Position).z;
            position = worldPoseOnManipulationStart.Rotation * position;
            constrainedMovement.z -= position.z;
        }
        // transform.Position = position;
    }


	

    public void OnPointerClicked(MixedRealityPointerEventData eventData) {}
}