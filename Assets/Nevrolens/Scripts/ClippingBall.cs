﻿using System;
using System.Linq;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.UI;
using Photon.Pun;
using TMPro;
using UnityEngine;

public class ClippingBall : MonoBehaviour
{
    [SerializeField] private Transform brainTransform;
    [SerializeField] private Transform clippingPlaneTransform;
    
    [Range(0.05f, 1.0f)]
    [SerializeField] private float ballDistance = 0.3f;

    [SerializeField] private Transform brainBoxTransform;
    private float scalingFactor = 1f;

    public void Update()
    {
        if (brainBoxTransform.hasChanged)
        {
            var boxScale = brainBoxTransform.localScale;
            scalingFactor = (boxScale.x + boxScale.y + boxScale.z) / 3f; // Using avg as this is maybe not correct mathematically, but mostly scaling should happen on all axes so it so it doesn't matter. 
            brainBoxTransform.hasChanged = false; 
        }
        
        var pos = transform.position;
        var dir = (brainTransform.position - pos).normalized; 
        
        clippingPlaneTransform.position = pos + dir * (ballDistance * scalingFactor); 
        
        clippingPlaneTransform.up = -dir;
    }
}
