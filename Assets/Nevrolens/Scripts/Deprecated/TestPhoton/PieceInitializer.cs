﻿using ExitGames.Client.Photon;
using Microsoft.MixedReality.Toolkit.UI;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
    
public class PieceInitializer : MonoBehaviourPunCallbacks
{
    private bool isInitialized = false;
    public override void OnPlayerPropertiesUpdate(Player targetPlayer, Hashtable changedProps)
    {
        if (changedProps.TryGetValue(BoardInitializer.firstPlayerFlag, out var isFirst))
        {
            SetPieceOwnership((bool) isFirst);
        }
    }

    private void SetPieceOwnership(bool isFirstPlayer)
    {
        if (isInitialized) return;
        
        
        // bool isFirstPlayer = (bool)photonView.Owner.CustomProperties[BoardInitializer.firstPlayerFlag];
        // bool isFirstPlayer = true;
    
        Renderer[] renderers = GetComponentsInChildren<Renderer>(true);
    
        for (int i=0;i<renderers.Length;i++)
        {
            if (isFirstPlayer)
            {
                renderers[i].material.color = new Color(0.9f, 0.0f, 0.0f);
            }
            else
            {
                renderers[i].material.color = new Color(0.1f, 0.1f, 0.1f);
            }
        }
    
        if (!photonView.IsMine)
        {
            GetComponent<ObjectManipulator>().enabled = false;
            GetComponent<ClickRecognizer>().enabled = false;
        }

        isInitialized = true;
    }
}
