﻿using System;
using Microsoft.MixedReality.Toolkit.UI;
using UnityEngine;

public class ClippingSystem : MonoBehaviour
{
    public ClippingPlane clippingPlane;

    public ClippingBall clippingBall;

    [Range(0.0f, 1.5f)] 
    [SerializeField] 
    public float ballHandelDistance = 0.3f;

    private PositionParent positionParent;


    public enum ClippingType
    {
        Disabled = 0,
        Legacy,
        Ball,
    };

    
    [NonSerialized]
    public ClippingType clippingType = ClippingType.Disabled;
    
    private bool isInManipulation;
    
    void Start()
    {
        clippingPlane.GetComponent<ObjectManipulator>().OnManipulationStarted.AddListener(OnManipulationStared);
        clippingPlane.GetComponent<ObjectManipulator>().OnManipulationEnded.AddListener(OnManipulationEnded);
        clippingBall.GetComponent<ObjectManipulator>().OnManipulationStarted.AddListener(OnManipulationStared);
        clippingBall.GetComponent<ObjectManipulator>().OnManipulationEnded.AddListener(OnManipulationEnded);
        positionParent = GetComponent<PositionParent>();
    }

    public ClippingType ToggleTriStateClipping()
    {

        ClippingType type;
        switch (clippingType)
        {
            case ClippingType.Ball:
                type = ClippingType.Legacy;
                break;
            case ClippingType.Legacy:
                type = ClippingType.Disabled;
                break;
            case ClippingType.Disabled:
                type = ClippingType.Ball;
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(clippingType), clippingType, null);
        }
        
        clippingType = type;
        return type;
    }
    
    public void ChangeClippingType(ClippingType type)
    {
        switch (type)
        {
           case ClippingType.Ball:
               clippingBall.gameObject.SetActive(true);
               clippingPlane.GetComponent<Renderer>().enabled = false;
               clippingPlane.GetComponent<Collider>().enabled = false;
               break;
           case ClippingType.Legacy:
               clippingBall.gameObject.SetActive(false);
               clippingPlane.GetComponent<Renderer>().enabled = true;
               clippingPlane.GetComponent<Collider>().enabled = true;
               break;
           case ClippingType.Disabled:
               break;
           default:
               throw new ArgumentOutOfRangeException(nameof(type), type, null);
        }
    }

    public void OnManipulationStared(ManipulationEventData eventData) => isInManipulation = true;
    public void OnManipulationEnded(ManipulationEventData eventData) => isInManipulation = false;

    void Update()
    {
        if (!isInManipulation && !positionParent.isMoving)
            return;
        clippingPlane.UpdateShader();
        
        // if (clippingType == ClippingType.Ball) 
        //     clippingBall.UpdatePlane();

    }
}
