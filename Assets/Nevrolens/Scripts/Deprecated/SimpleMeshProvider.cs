﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleMeshProvider : MonoBehaviour
{
    [SerializeField] private List<MeshFilter> meshes;
    
    // Start is called before the first frame update
    void Start()
    {
        var colliders = GetComponentsInChildren<MeshCollider>();

        for (var i = 0; i < meshes.Count; i++)
        {
            
            var m = meshes[i];
            var c = colliders[i];

            if (m.name == c.name)
            {
                c.sharedMesh = m.sharedMesh;
            }
            else
                throw new Exception($"{m.name} not equal {c.name}");
            
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
