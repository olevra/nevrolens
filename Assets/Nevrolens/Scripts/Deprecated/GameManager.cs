﻿using System;
using Microsoft.MixedReality.Toolkit;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.Input;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    
    public BrainSystem brainSystem;
    public GameObject debugContent;
    
    
    public float brainSpawnHeight = 0.4f;

    [SerializeField] private bool showProfiler = false;
    [SerializeField] private bool rotateBrainOnSpawn = false;
    [SerializeField] private bool spawnInAir = true;
    [SerializeField] private float maxSpawnInAirDistance = 1.5f;
    [SerializeField] private bool spawnOnStart = true;


    internal const int BrainPartLayer = 8;
    internal const int FullBrainLayer = 9;
    internal const int SpatialAwarenessLayer = 31; //todo: is there another way to get this value?

    private void Awake()
    {
        // brainSystem.gameObject.SetActive(false);
        Debug.developerConsoleVisible = false;
        CoreServices.DiagnosticsSystem.ShowProfiler = showProfiler; 
        
        if (!Application.isEditor)
            debugContent.SetActive(false);
            
    }

    private void Start()
    {
        if (spawnOnStart)
        {
            var ray = new Ray(Camera.main.transform.position, Camera.main.transform.forward);
            var pos = Camera.main.transform.position + Camera.main.transform.forward.normalized * maxSpawnInAirDistance;
            if (Physics.Raycast(ray, out var hit, maxSpawnInAirDistance))
                pos = hit.point;
            
            brainSystem.SpawnBrain(pos, 0, rotateBrainOnSpawn);
            
        }
    }

    public void OnClickReset()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void OnGlobalPointerDown(MixedRealityPointerEventData eventData)
    {

        if (eventData.Pointer?.Result?.CurrentPointerTarget == null)
        {
            if (brainSystem.UnselectIfSelected()) 
                brainSystem.UpdateSelectedMenu();
            if (spawnInAir)
            {
                var ray = new Ray(Camera.main.transform.position, Camera.main.transform.forward);
                var pos = Camera.main.transform.position + Camera.main.transform.forward.normalized * maxSpawnInAirDistance;
                if (Physics.Raycast(ray, out var hit, maxSpawnInAirDistance))
                    pos = hit.point;
                
                brainSystem.SpawnBrain(pos, 0, rotateBrainOnSpawn);
                
            }
            return;
        }

        switch (eventData.Pointer.Result.CurrentPointerTarget.layer)
        {
            case SpatialAwarenessLayer:
            {
                brainSystem.UnselectIfSelected();
                
                var hitPoint = eventData.Pointer.Result.Details.Point; 
                brainSystem.SpawnBrain(hitPoint, brainSpawnHeight, rotateBrainOnSpawn);
                
                break;
            }
        }
    }

}
