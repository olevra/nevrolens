using System;
using UnityEngine;

public class ClippingPlane : MonoBehaviour {


	private BrainSystem brainSystem;
	private Plane plane;
	private Transform t;
	private static readonly int Plane = Shader.PropertyToID("_Plane");
	private Vector4 planeRepresentation;
	private static readonly int ClippingEnable = Shader.PropertyToID("_ClippingEnable");


	private void OnEnable()
	{
		foreach (var b in brainSystem.brainParts)
		{
			// b.Material.SetInt(ClippingEnable, 1);
			throw new Exception();
		}
	}

	private void OnDisable()
	{
		foreach (var b in brainSystem.brainParts)
		{
			// b.Material.SetInt(ClippingEnable, 0);
			throw new Exception();
		}
	}

	private void Awake()
	{
		brainSystem = GetComponentInParent<BrainSystem>();
		t = transform;
		var up = t.up;
		planeRepresentation = new Vector4(up.x, up.y, up.z, -Vector3.Dot(up, t.position));
		UpdateShader();
	}
	

	public void UpdateShader()
	{
		var up = t.up;
		planeRepresentation.x = up.x;
		planeRepresentation.y = up.y;
		planeRepresentation.z = up.z;
		planeRepresentation.w = -Vector3.Dot(up, t.position);

		foreach (var b in brainSystem.brainParts)
		{
			// b.Material.SetVector(Plane, planeRepresentation);
			throw new Exception();
		}
	}
}
