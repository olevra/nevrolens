﻿using System;
using System.Linq;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.UI;
using Microsoft.MixedReality.Toolkit.UI.BoundsControl;
using Microsoft.MixedReality.Toolkit.Utilities.Solvers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BrainSystem : MonoBehaviour
{

    [SerializeField] public GameObject brain;
    [SerializeField] public BrainPart[] brainParts;
    
    [SerializeField] public GameObject featureMenu;
    [SerializeField] public TextMeshPro menuTitle;
    
    [SerializeField] public ClippingSystem clippingSystem;

    [SerializeField] public BoundsControl boundsControl;

    public ClippingPlane ClippingPlane => clippingSystem.clippingPlane;
    
    [SerializeField] public Interactable movableButton;
    [SerializeField] public Interactable transparencyButton;
    [SerializeField] public Interactable clippingButton;
    
    [SerializeField] public Interactable palmBoundingButton;
    [SerializeField] public Interactable palmClippingButton;
    [SerializeField] public Interactable palmMovableButton;
    
    
    public bool isClippingEnabled = false;
    public bool isBrainMovableEnabled = false;
    public bool isBoundingBoxEnabled = true;

    
    public float menuSpawnHeight = 0.05f;
    // public float menuSpawnScale = 1.6f;
    [Tooltip("Should menu spawn on XZ planar hit on brain collider. Bad description, I know... Just set it to true.")]
    [SerializeField] private bool menuSpawnHorizontalBound = true;

    [NonSerialized] public BrainPart selectedBrainPart;
    
    public enum MovableState
    {
        Teleport,
        Drag,
        Disabled,
    }

    public MovableState movableState = MovableState.Teleport;
    private CapsuleCollider brainMenuCollider;

    void Awake()
    {
        brainParts = brain.GetComponentsInChildren<BrainPart>();
        movableButton.IsToggled = false;
        transparencyButton.IsToggled = false;
        clippingButton.IsToggled = isClippingEnabled;
        featureMenu.SetActive(false);
        
        // UpdateClippingState(ClippingSystem.ClippingType.Disabled);
        UpdateMovableState(MovableState.Drag);
        UpdateBoundingBoxState(isBoundingBoxEnabled);
    }

    private void Start()
    {
        brainMenuCollider = brain.GetComponentInChildren<CapsuleCollider>();
        Debug.Assert(brainMenuCollider.gameObject.name == "MenuCapsule");
        brainMenuCollider.enabled = false;
        UpdateClippingState(ClippingSystem.ClippingType.Disabled);
        // clippingSystem.gameObject.SetActive(false);
    }

    void UpdateBoundingBoxState(bool state)
    {
        isBoundingBoxEnabled = state; 
        boundsControl.enabled = isBoundingBoxEnabled;
        
        if (isBoundingBoxEnabled && !isBrainMovableEnabled)
            UpdateMovableState(MovableState.Drag);
        
        if (!isBoundingBoxEnabled)
            UpdateMovableState(MovableState.Disabled);
    }

    void UpdateClippingState(ClippingSystem.ClippingType state)
    {
        
        clippingSystem.ChangeClippingType(state);
        
        isClippingEnabled = clippingSystem.clippingType != ClippingSystem.ClippingType.Disabled;
        clippingSystem.gameObject.SetActive(isClippingEnabled);
        
        UpdatePalmClippingButton();
    }
    
    void UpdatePalmClippingButton()
    {
        var s = clippingSystem.clippingType.ToString();
        palmClippingButton.GetComponent<ButtonConfigHelper>().MainLabelText = $"Clipping\n{s}";
        palmClippingButton.IsToggled = isClippingEnabled;
    }
    
    void UpdatePalmMovableButton()
    {
        palmMovableButton.GetComponent<ButtonConfigHelper>().MainLabelText = $"Movable\n{movableState.ToString()}";
        palmMovableButton.IsToggled = isBrainMovableEnabled;
    }

    void SetBrainPartMovable(bool isMovable, bool butActually = false, bool brainPartEnable = true)
    {
        var brianParts = brain.GetComponentsInChildren<BrainPart>();

        foreach (var b in brianParts)
        {
            if (b.isMovable != isMovable)
            {
                b.isMovable = isMovable;
                b.GetComponent<ObjectManipulator>().enabled = butActually;
            }

            b.enabled = brainPartEnable;

        }
        UpdateSelectedMenu(changePosition: false);
    }

    public void TapBrain(
        GameObject tapTarget, 
        Vector3 tapPosition
        )
    {
        
        SelectBrainPart(tapTarget);
        
        var dir = (tapPosition - Camera.main.transform.position).normalized;
        if (menuSpawnHorizontalBound) dir.y = 0;
        
        var spawnHeight = FindMenuOnBoundsPosition(tapPosition, dir, heightOffset: menuSpawnHeight);
        UpdateSelectedMenu(spawnHeight);
    }

    public void OnTransparentClick()
    {
        if (selectedBrainPart == null)
            return;
        // transparencyButton.IsToggled = selectedBrainPart.ToggleTransparencyAndCollider();
        throw new Exception();
    }
    

    public void OnPalmClippingClick()
    {
        var state = clippingSystem.ToggleTriStateClipping();
        UpdateClippingState(state);
    }
    
    public void OnMovableClick()
    {
        if (selectedBrainPart == null)
            return;

        var objectManipulator = selectedBrainPart.GetComponent<ObjectManipulator>();
        
        selectedBrainPart.isMovable = !selectedBrainPart.isMovable;
        objectManipulator.enabled = selectedBrainPart.isMovable;
    }

    public void UpdateMovableState(MovableState state)
    {
        movableState = state;
        isBrainMovableEnabled = movableState != MovableState.Disabled;
        
        switch (movableState)
        {
            case MovableState.Teleport:
                brain.GetComponent<ObjectManipulator>().enabled = false;
                SetBrainPartMovable(true, brainPartEnable: true);
                break;
            case MovableState.Drag:
                brain.GetComponent<ObjectManipulator>().enabled = true;
                SetBrainPartMovable(false, brainPartEnable: false);
                break;
            case MovableState.Disabled:
                brain.GetComponent<ObjectManipulator>().enabled = false;
                SetBrainPartMovable(true, brainPartEnable: true);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
        
        UpdatePalmMovableButton();
    }
    
    public void ToggleTriStateMovable()
    {
        MovableState newState;
        
        switch (movableState)
        {
            case MovableState.Teleport:
                newState = MovableState.Disabled;
                break;
            case MovableState.Drag:
                newState = MovableState.Teleport;
                break;
            case MovableState.Disabled:
                newState = MovableState.Drag;
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
        UpdateMovableState(newState);
    }
    
    public void OnPalmMovableClick()
    {
        ToggleTriStateMovable();
    }
    
    public void ToggleBoundingBox()
    {
        isBoundingBoxEnabled = !isBoundingBoxEnabled;
        UpdateBoundingBoxState(isBoundingBoxEnabled);
    }

    public Vector3 FindMenuOnBoundsPosition(Vector3 throughPosition, Vector3 viewDirection, float heightOffset = 0.1f)
    {
        const float maxCastDistance = 10f;
        brainMenuCollider.enabled = true;
        var height = brainMenuCollider.bounds.max.y + heightOffset;


        var outsidePos = throughPosition + viewDirection.normalized * maxCastDistance;
        var ray = new Ray(outsidePos, -viewDirection);
        
        var hitPos = throughPosition;
        const int layerMask = 1 << GameManager.FullBrainLayer;
        if (Physics.Raycast(ray, out var hit, maxCastDistance, layerMask))
            hitPos = hit.point;
        else if (height < throughPosition.y)
            height = throughPosition.y + heightOffset * 2;
            
            
        brainMenuCollider.enabled = false;
        
        var menuPosition = new Vector3(hitPos.x, height, hitPos.z);
        return menuPosition;

    }
    public Vector3 GetMaxBrainLocalHeight(Vector3 axis = default, Func<float, float, bool> maxFunc = default)
    {
        if (maxFunc == default)
            maxFunc = (a, b) => a > b;
        if (axis == default)
            axis = Vector3.up;
        
        var maxPosition = brainParts[0].localVisualPosition;
            
        foreach (var b in brainParts.Skip(1))
        {
            var selectedValue = Vector3.Dot(b.localVisualPosition, axis);
            var maxValue = Vector3.Dot(maxPosition, axis);
            
            if (maxFunc(selectedValue, maxValue))
                maxPosition = b.localVisualPosition;
        }

        return maxPosition;

    }

    public bool UnselectIfSelected()
    {
        if (!isActiveAndEnabled || !HasSelectedBrainPart()) return false;
        
        UnselectBrianPart();
        return true;

    }

    public void SpawnBrain(Vector3 position, float heightOffset, bool rotateBrainOnSpawn = true)
    {

        if (movableState == BrainSystem.MovableState.Teleport)
        {
            gameObject.SetActive(true); 
            var brainTransform = transform;
            brainTransform.position = position + Vector3.up * heightOffset;
            
            if (rotateBrainOnSpawn)
            {
                var pos = Camera.main.transform.position;
                pos.y = brainTransform.position.y; 
                brainTransform.LookAt(pos);
                brainTransform.Rotate(Vector3.up, 90);
            }
        }

    }

    public void UpdateSelectedMenu(Vector3 position = default, bool changePosition = true)
    {
        if (selectedBrainPart == null)
        {
           featureMenu.SetActive(false);
           return; 
        }
        
        featureMenu.SetActive(true);
        menuTitle.text = selectedBrainPart.DisplayName;
        movableButton.IsToggled = selectedBrainPart.isMovable;
        transparencyButton.IsToggled = selectedBrainPart.isTransparent;
        clippingButton.IsToggled = isClippingEnabled;
        if (changePosition && !featureMenu.GetComponent<RadialView>().isActiveAndEnabled) 
            featureMenu.transform.position = position;
    }

    public bool IsSelectedBrainPart(BrainPart brainPart) => selectedBrainPart != null && selectedBrainPart == brainPart;

    public bool HasSelectedBrainPart() => selectedBrainPart != null;

    public bool SelectBrainPart(GameObject brainPart) => SelectBrainPart(brainPart.GetComponent<BrainPart>());
    public bool SelectBrainPart(BrainPart brainPart)
    {
        if (IsSelectedBrainPart(brainPart)) 
            return false;
        
        UnselectBrianPart();
        
        // brainPart.SetSelected();
        selectedBrainPart = brainPart;
        return true;
    }

    public void UnselectBrianPart()
    {
        if (selectedBrainPart == null)
            return;
        
        // selectedBrainPart.SetUnselected();
        selectedBrainPart = null;
    }
}
