﻿using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.MixedReality.Toolkit.Utilities.Solvers;
using UnityEngine;

public class HandMenu : MonoBehaviour
{
    // Start is called before the first frame update
    private void Awake()
    {
        var runtimePlatform = GlobalSettings.Instance.RuntimePlatform;
        if (runtimePlatform != GlobalSettings.SupportedPlatform.HoloLens2)
        {
            GetComponent<HandConstraintPalmUp>().enabled = false;
        }



    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
