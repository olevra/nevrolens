﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class NetworkPlayerSpawner : NetworkSpawner
{
	protected override void InitializeNetworkObject()
	{
		InitialRPCs("SetParent", "SetColor", "SetText", "SetNameTagLookAt");
	}

	public void OnNickNameChanged()
	{
		RPC("SetText", RpcTarget.AllBuffered);
	}

	protected override void UpdateNetworkObject()
	{
		UpdateGameSpaceTransform(cameraTransform.position, cameraTransform.rotation);
	}
}
