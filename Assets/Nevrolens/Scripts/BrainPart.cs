﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.UI;
using Microsoft.MixedReality.Toolkit.Utilities;
using Nevrolens.Scripts.Experimental;
using UnityEngine;
using static Nevrolens.Helpers.Helpers;
using Random = UnityEngine.Random;

[RequireComponent(typeof(SnapInPlace))]
[RequireComponent(typeof(ObjectManipulator))]
public class BrainPart : MonoBehaviour, IMixedRealityPointerHandler
{
    [NonSerialized] private bool isSelected = false;
    [NonSerialized] public bool isTransparent = false;
    [NonSerialized] public bool isMovable = false;
    [NonSerialized] public bool isCollidable = true;
    [NonSerialized] public Color color;


    public string DisplayName
    {
        get
        {
            if (displayName == string.Empty)
                displayName = PrettyName(name);
            return displayName;
        }
    }

    [NonSerialized] public Vector3 localVisualPosition;
    
    [NonSerialized] public NiftiLabel label;


    private GameObject visualAnchor;

    private Mesh brainPartMesh;

    public Mesh BrainPartMesh
    {
        get
        {
            if (!brainPartMesh)
            {
                var mesh = GetComponent<MeshFilter>().sharedMesh;
                brainPartMesh = new Mesh
                {
                    vertices = mesh.vertices,
                    triangles = mesh.triangles,
                    uv = mesh.uv
                };
            }
                
            return brainPartMesh;
        } 
    } 

    public bool IsSelected
    {
        get => isSelected;
        set
        {
            if (value == true)
                BrainPartRenderer.material.EnableKeyword("_RIM_LIGHT");
            else 
                BrainPartRenderer.material.DisableKeyword("_RIM_LIGHT");
            isSelected = value;
        }
    }

    public Renderer BrainPartRenderer
    {
        get
        {
            if (brainPartRenderer == null)
                brainPartRenderer = GetComponent<Renderer>();
            return brainPartRenderer;
        }
    }

    private Renderer brainPartRenderer;
    private static readonly int Color = Shader.PropertyToID("_Color");
    private static readonly int CutoffColor = Shader.PropertyToID("_CutoffColor");
    private static readonly int RimEnable = Shader.PropertyToID("_RimLight");
    private static readonly int RimColor = Shader.PropertyToID("_RimColor");

    [NonSerialized] private string displayName = string.Empty;

    [NonSerialized] public SnapInPlace SnapInPlace;

    private void OnDisable()
    {
        IsSelected = false;
    }

    private void Start()
    {
        // need to be in Start, I guess bounds.center is not calculated before.
        localVisualPosition = BrainPartRenderer.bounds.center - transform.position;
        GetComponent<ObjectManipulator>().OnManipulationStarted.AddListener(OnManipulationStarted);

        SnapInPlace = GetComponent<SnapInPlace>(); 
        // SnapInPlace.enabled = false;

        var toolTipSpawner = GetComponent<Nevrolens.ToolTipSpawner>();

        if (toolTipSpawner != null)
        {
            toolTipSpawner.ToolTipText = DisplayName;
            visualAnchor = new GameObject($"Anchor {DisplayName}");
            visualAnchor.transform.parent = transform; 
            visualAnchor.transform.position = transform.position + localVisualPosition;
            // anchor.GetComponent<MeshRenderer>().enabled = false;
            toolTipSpawner.Anchor = visualAnchor.transform;
        } 
        FindNiftiColor(); 
        
    }

    void FindNiftiColor()
    {
        if (label.index != 0)
        {
            // SetColor(UnityEngine.Color.HSVToRGB(label.index / 115f, 1, 1));
            SetColor(label.color);
            return;
        }
        
        Debug.LogError($"No label found for {displayName}. You should look into why, the code below is shit and should not be executed.");

    }

    public void SetColor() => FindNiftiColor();

    public void SetColor(Color color)
    {
        
        var matBlock = new MaterialPropertyBlock();
        matBlock.SetVector(Color, color);
        matBlock.SetVector(CutoffColor, color );
        matBlock.SetVector(RimColor, UnityEngine.Color.white);
        
        BrainPartRenderer.SetPropertyBlock(matBlock);
        
        this.color = color;
    } 

    private void OnManipulationStarted(ManipulationEventData eventData)
    {
        // SnapInPlace.enabled = true;
        
    }
    
    public void OnPointerDown(MixedRealityPointerEventData eventData)
    {
        NetworkBrainManager.Instance.SelectedBrainPart = this;
    }

    public void OnPointerDragged(MixedRealityPointerEventData eventData)
    {
    }

    public void OnPointerUp(MixedRealityPointerEventData eventData)
    {
        
    }

    public void OnPointerClicked(MixedRealityPointerEventData eventData)
    {
        
    }
}
