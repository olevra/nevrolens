﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.MixedReality.Toolkit.UI;
using Microsoft.MixedReality.Toolkit.UI.BoundsControl;
using Nevrolens.Scripts.Experimental;
using Photon.Pun;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using ToolTipSpawner = Nevrolens.ToolTipSpawner;

public class NetworkBrainManager : MonoBehaviour
{
    private BrainPart[] brainParts;

    public BrainPart[] BrainParts
    {
        get
        {
           if (brainParts == null) 
            brainParts = brain.GetComponentsInChildren<BrainPart>();
           return brainParts;
        }
    }

    public Dictionary<string, BrainPart> NamedBrainParts { get; private set; }

    [SerializeField] public NetworkClippingSystem clippingSystem;
    [SerializeField] private TextMeshPro brainTitleText;
    [SerializeField] public GameObject brain;
    [SerializeField] private NetworkAdmin admin;
    [SerializeField] private BoundsControl gameWorldSpace;
    [SerializeField] public NiftiPlane niftiPlane;

    [SerializeField] public TextAsset niftiLabelFile;

    private const string brainTitle = "Waxholm Space Rat Brain";

    
    public static NetworkBrainManager Instance { get; private set; }

    public string BrainTitle
    {
        get => brainTitleText.text;
        set => brainTitleText.text = value;
    }

    public void ResetBrainTitle() => BrainTitle = brainTitle;

    public UnityEvent onSelectedBrainPart = new UnityEvent();

    public UnityEvent onNickNameChanged = new UnityEvent();

    private BrainPart selectedBrainPart;

    private List<NiftiLabel> labels;

    public List<NiftiLabel> Labels
    {
        get
        {
            if (labels == null)
                labels = NiftiLabelParser.Parse(niftiLabelFile.text);
            return labels;
        } 
    }

    public BrainPart SelectedBrainPart
    {
        get => selectedBrainPart;
        set
        {
            if (selectedBrainPart == value)
                return;
            
            if (selectedBrainPart != null)
                selectedBrainPart.IsSelected = false;
            
            selectedBrainPart = value;
            
            if (selectedBrainPart != null)
                selectedBrainPart.IsSelected = true;
            
            onSelectedBrainPart.Invoke();
        }
    }

    public NetworkAdmin Admin => admin;

    public BoundsControl GameWorldSpace => gameWorldSpace;


    
    public void CleanUp()
    {
        foreach (var brainPart in BrainParts)
        {
            brainPart.GetComponent<SnapInPlace>().Snap(playSound: false);
        }
        
    }

    public void TooltipToggle(bool active)
    {
       foreach (var brainPart in BrainParts)
       {
           brainPart.GetComponent<ToolTipSpawner>().enabled = active;
       } 
    }

    public void SetColors(IEnumerable<BrainPart> brainParts = null, bool useCustomColor = false, float min = 0f, float max = 1f, bool reverse = false)
    {
        if (brainParts == null)
            brainParts = BrainParts;
        
        if (useCustomColor)
            ChangeColor.SetColors(brainParts, min, max, reverse);
        else
            foreach (var brainPart in brainParts)
                brainPart.SetColor();

        clippingSystem.UpdateRenderers();
    }

    private void Start()
    {
        NamedBrainParts = BrainParts.ToDictionary(
            b => b.name,
            b => b
        );
        
        foreach (var bp in BrainParts)
        {
            bp.label = Labels
                .Where(lab => lab.description.Contains(bp.DisplayName.ToLower()))
                .OrderBy(lab => Math.Abs(lab.description.Length - bp.DisplayName.Length))
                .FirstOrDefault(); 

        }

    }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Debug.LogError("Multiple BrainManagers");
            Destroy(this);
            return;
        }


        // SetColors();
        ResetBrainTitle();
    }
    
}
