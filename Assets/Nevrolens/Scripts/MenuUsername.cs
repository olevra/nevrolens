﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using TMPro;
using UnityEngine;

public class MenuUsername : MonoBehaviour
{
    [SerializeField] private TextMeshPro usernameText;

    private void Start()
    {
        if (!PhotonNetwork.IsConnected)
        {
            usernameText.gameObject.SetActive(false);
            gameObject.SetActive(false);
            return;
        }
        OnNickNameChanged();
        usernameText.color = NetworkGameSpaceObject.GetPlayerColor(PhotonNetwork.LocalPlayer.ActorNumber);
    }

    public void OnNickNameChanged()
    {
        usernameText.text = PhotonNetwork.LocalPlayer.NickName;
    }
}
