﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Nevrolens.Helpers;
using NiftiNET;
using UnityEditor;
using UnityEngine;

namespace Nevrolens.Scripts
{
    public class BuildNifti3DTexture : MonoBehaviour
    {

        [SerializeField] private TextAsset jsonFile;
        [SerializeField] private TextAsset datFile;
        
        [SerializeField] private string outputPath = "Assets/"; 
        [Tooltip("If loading a JSON-file, should it be serialized as dat-file to improve access time later.")]
        [SerializeField] private bool jsonWriteToDat = true;
        
        private Texture3D niftiTexture;
        private SerializableNifti nifti;
        private string niftiName;
        
        [Tooltip("Scale of compression for texture, should be a power of 2 (1, 2, 4, 8...")]
        [SerializeField] private int scalingFactor = 1;
        private int w;
        private int h;
        private int d;

        private byte[] colors;


        private bool done = false; 
        private int ww;
        private int hh;
        private int dd;

        void FromSerializedNifti()
        {
            w = nifti.Dimensions[0];
            h = nifti.Dimensions[1];
            d = nifti.Dimensions[2];
            ww = w / scalingFactor;
            hh = h / scalingFactor;
            dd = d / scalingFactor;
            
            niftiTexture = new Texture3D(ww, hh, dd, TextureFormat.Alpha8, false) {wrapMode = TextureWrapMode.Clamp, filterMode = FilterMode.Point };
            colors = new byte[ww * hh * dd];

        }

        private void Start()
        {
            Debug.Log($"Staring deserializing:");
            if (jsonFile != null)
            {
                Debug.Log("Deserializing from JSON...");
                nifti = NevrolensNifti.JsonDeserialize(jsonFile.text);
                niftiName = jsonFile.name;
                if (jsonWriteToDat)
                {
                    NevrolensNifti.SerializeNow(nifti, $"Assets/{niftiName}.dat");
                }
            }
            else if (datFile != null)
            {
                Debug.Log("Deserializing from dat...");
                niftiName = datFile.name;
                var s = new MemoryStream(datFile.bytes);
                nifti = NevrolensNifti.DeSerializeNow<SerializableNifti>(s);
            }
            else
            {
                Debug.LogWarning("No file found...");
                done = true;
                return;
            }
            
            Debug.Log($"Deserialized Nifti {niftiName} with dimensions {nifti.Dimensions.ToList()}");
            FromSerializedNifti();
        }

        private void Update()
        {
            if (!done && ToTexOnUpdate(Time.frameCount))
            {
                done = true;
                
                Debug.Log("Successfully loaded array. Filling texture...");
                
                niftiTexture.SetPixelData(colors, 0);
                niftiTexture.Apply();
                
                Debug.Log("Creating texture asset...");
                
                #if UNITY_EDITOR
                var outPath = outputPath;
                if (string.IsNullOrWhiteSpace(outPath))
                    outPath = "Assets/"; 
                
                if (!outputPath.EndsWith("/") && !outputPath.EndsWith("\\"))
                    outPath += "/";

                var format = niftiTexture.format == TextureFormat.Alpha8 && niftiTexture.filterMode == FilterMode.Point
                    ? "A8P"
                    : "XX";  // if you want to use some other formats, this should probaly be expanded.

                AssetDatabase.CreateAsset(niftiTexture, $"{outPath}{niftiName.Replace(".", "_")}_SF{scalingFactor}_{format}.asset");
                #endif
                
                Debug.Log("Done! You can shut down the program.");
            }
                
        }


        bool ToTexOnUpdate(int frame)
        {
            Debug.Log($"Frame {frame}");
            
            if (frame >= niftiTexture.depth - 1)
                return true;

            var size = w * h;
            var niftiFrame = (IList<ushort>) new ArraySegment<ushort>(nifti.Data, size * frame * scalingFactor, size);

            for (var y = 0; y < hh; y++)
            {
                for (var x = 0; x < ww; x++)
                {
                    var value = niftiFrame[x * scalingFactor + y * scalingFactor * w];
                    // var r = (byte) (value >> 8);
                    var g =  (byte) (value & 0xff);
                    // var col = new Color32(r, g, 0, 0);
                    // niftiTexture.SetPixel(x, y, frame, col);
                    colors[x + y * ww + frame * ww * hh] = g; 
                }
            }

            return false;

        }
        
        
        
    }
}
