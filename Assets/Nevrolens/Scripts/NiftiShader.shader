﻿// Upgrade NOTE: replaced 'UNITY_INSTANCE_ID' with 'UNITY_VERTEX_INPUT_INSTANCE_ID'

// Upgrade NOTE: replaced 'UNITY_INSTANCE_ID' with 'UNITY_VERTEX_INPUT_INSTANCE_ID'

Shader "Unlit/NiftiShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
//        _NiftiColorLookupTable ("Mapping Nifti Texture value to a color", 1D) = "white" {}
        _NiftiTex ("Nifti Texture 3d", 3D) = "black" {} 
        _VoidValue ("Alpha level of empty texels", float) = 0.07
        _NiftiAlphaValue ("Alpha level of filled texels", float) = 0.9
        _ColorMode ("Color modes", float) = 2
        _NiftiColorLookupTableLength ("Length of lookup table", float) = 115.0
    }
    SubShader
    {
        Tags {"Queue" = "Transparent" "RenderType"="Transparent"  }
        LOD 100
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha
        
        Pass
        {
            CGPROGRAM
            #pragma vertex vert alpha
            #pragma fragment frag alpha
            // make fog work
            #pragma multi_compile_fog
            #pragma multi_compile_instancing

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
                UNITY_VERTEX_INPUT_INSTANCE_ID
                UNITY_VERTEX_OUTPUT_STEREO
            };

            float _VoidValue;
            float _NiftiAlphaValue;
            float _NiftiColorLookupTableLength;
            int _ColorMode;
            sampler2D _MainTex;
            uniform sampler1D _NiftiColorLookupTable;
            float4 _MainTex_ST;
            
            
            sampler3D _NiftiTex;
            uniform float4x4 _PlaneTransformMatrix;
            
            UNITY_INSTANCING_BUFFER_START(Props)
                // put more per-instance properties here
            UNITY_INSTANCING_BUFFER_END(Props)
            
            float3 HSVToRGB( float3 c )
            {
                float4 K = float4( 1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0 );
                float3 p = abs( frac( c.xxx + K.xyz ) * 6.0 - K.www );
                return c.z * lerp( K.xxx, saturate( p - K.xxx ), c.y );
            }
            v2f vert (appdata v)
            {
                v2f o;
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_TRANSFER_INSTANCE_ID(v, o);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                UNITY_SETUP_INSTANCE_ID(i);
                float4 uvw = mul(_PlaneTransformMatrix, float4(i.uv, 0, 1));
                
                fixed4 col = tex3D (_NiftiTex, uvw);
                if (col.a > 0)
                {
                    
                    if (_ColorMode == 0)
                    {
                        col.rgb = col.aaa;
                        col.a = _NiftiAlphaValue;
                    }
                    else if (_ColorMode == 1)
                    {
                        col.rgb = HSVToRGB(float3((col.a * 256.0 / 115.0) , 1, 1));
                        col.a = _NiftiAlphaValue;
                    }
                    else if (_ColorMode == 2)
                    {
                        fixed4 c = tex1D(_NiftiColorLookupTable, col.a * 256.0 / _NiftiColorLookupTableLength);
                        col.rgb = c.rgb;
                        col.a = c.a;
                    }
                    
                }
                else
                    col.a = _VoidValue;
                
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
