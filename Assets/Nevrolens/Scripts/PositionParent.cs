﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionParent : MonoBehaviour
{

    [SerializeField] private Transform parent;
    private Vector3 parentInitialPosition;
    private Vector3 initialOffset;

    [NonSerialized] public bool isMoving;

    // Start is called before the first frame update
    void Start()
    {
        parentInitialPosition = parent.position;
        initialOffset = transform.position - parentInitialPosition;


    }

    // Update is called once per frame
    void Update()
    {

        var lastPos = transform.position;
        var newPos = parent.position + initialOffset;
        transform.position = newPos;
        isMoving = !lastPos.Equals(newPos);


    }
}
