﻿using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Input;
using Photon.Pun;
using UnityEngine;

public class NetworkPointerSpawner : NetworkSpawner
{
    private const int BrainPartLayer = 8;
    private IMixedRealityPointer pointer;
    private bool isNetworkObjectActive;

    protected override void InitializeNetworkObject()
    {
		InitialRPCs("SetParent", "SetColor");
        SetNetworkObjectActive(false, force: true);
    }

    protected override void UpdateNetworkObject()
    {
        if (pointer == null || !pointer.Result.CurrentPointerTarget || pointer.Result.CurrentPointerTarget.layer != BrainPartLayer)
        {
            SetNetworkObjectActive(false);
        }
        else
        {
            SetNetworkObjectActive(true);
            UpdateGameSpaceTransform(pointer.Result.Details.Point, Quaternion.LookRotation(cameraTransform.position - pointer.Result.Details.Point, Vector3.up));
        }

    }

    private void SetNetworkObjectActive(bool active, bool force = false)
    {
        if (!force && isNetworkObjectActive == active) return;

        isNetworkObjectActive = active;
        RPC("SetActive", RpcTarget.All, active);
    }

    private void OnPrimaryPointerChanged(IMixedRealityPointer oldPointer, IMixedRealityPointer newPointer)
    {
        pointer = newPointer;
    }
        
    private void OnEnable()
    {
        var focusProvider = CoreServices.InputSystem?.FocusProvider;
        focusProvider?.SubscribeToPrimaryPointerChanged(OnPrimaryPointerChanged, true);
    }
    private void OnDisable()
    {
        var focusProvider = CoreServices.InputSystem?.FocusProvider;
        focusProvider?.UnsubscribeFromPrimaryPointerChanged(OnPrimaryPointerChanged);
        
        // This flushes out the current primary pointer
        OnPrimaryPointerChanged(null, null);
    }

}