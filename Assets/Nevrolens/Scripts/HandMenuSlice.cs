﻿using System.Collections;
using System.Collections.Generic;
using Microsoft.MixedReality.Toolkit.Utilities;
using TMPro;
using UnityEngine;


public class HandMenuSlice : MonoBehaviour
{

    [SerializeField] private TextMeshPro textMesh;
    [SerializeField] private GridObjectCollection buttonCollection;
    [SerializeField] private string title;

    public string Title
    {
        get => textMesh.text;
        set => textMesh.text = value;
    }
    // Start is called before the first frame update
    void Start()
    {
        if (title != null && title != "")
            Title = title;

    }
    
    

    // Update is called once per frame
    void Update()
    {
        
    }
}
