﻿using System;
using System.Linq;
using Microsoft.MixedReality.Toolkit;
using Photon.Pun;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

namespace Nevrolens.Helpers
{
    public static class Helpers
    {
        public static float Map (this float value, float fromSource, float toSource, float fromTarget, float toTarget)
        {
            return (value - fromSource) / (toSource - fromSource) * (toTarget - fromTarget) + fromTarget;
        }

        public static string Style(this string text, string style)
        {
            return $@"<style=""{style}"">{text}</style>";
        }
        public static string Newline(this string text, int numberOfNewlines = 1)
        {
            var newlines = string.Join(string.Empty, Enumerable.Repeat(Environment.NewLine, numberOfNewlines)); 
            return $"{text}{newlines}";
            
        }

        public static Vector4 ToVec4(this Vector3 value, float w = 0)
        {
            return new Vector4(value.x, value.y, value.z, w);
        }

        public static Vector3 ScalarInvert(this Vector3 scale)
        {
            return new Vector3(1 / scale.x, 1 / scale.y, 1 / scale.z);
        }

        public static Vector3 ScalarDivide(this Vector3 value, Vector3 other)
        {
            return new Vector3(value.x / other.x, value.y / other.y, value.z / other.z);
            
        }

        public static Vector3 ScalarMultiply(this Vector3 value, Vector3 other)
        {
            return new Vector3(value.x * other.x, value.y * other.y, value.z * other.z);
        }

        public static Vector4 ScalarMultiply(this Vector4 value, Vector3 other)
        {
            return new Vector4(value.x * other.x, value.y * other.y, value.z * other.z, value.w);
        }
        public static void Swap<T> (ref T lhs, ref T rhs) {
            T temp = lhs;
            lhs = rhs;
            rhs = temp;
        }

        public static T GetRandom<T>(this T[] array)
        {
            return array[Random.Range(0, array.Length)];
        }
        
        
        public static string ToTitleCase(string s)
        {
            string ToTitleCaseWord(string stringToConvert)
            {
                var firstChar = stringToConvert[0].ToString();
                return (stringToConvert.Length>0 ? firstChar.ToUpper()+stringToConvert.Substring(1) : stringToConvert);
            }

            return string.Join(" ", s.Split(' ').Select(ToTitleCaseWord));
        }

        public static string PrettyName(string name)
        {
            var prettyName = name.Replace("_", " ");
            prettyName = prettyName.Replace("-", " ");
            prettyName = prettyName.Replace("Fixed", "");
            prettyName = prettyName.Trim();
            prettyName = ToTitleCase(prettyName);
            return prettyName;
        }
    }
}
public class GlobalSettings : MonoBehaviour
{
    [SerializeField] private bool showProfiler;

    private static GlobalSettings instance = null;
    public static GlobalSettings Instance
    {
        get => instance;
    }


    public enum SupportedPlatform
    {
        Unsupported = 0,
        Android, HoloLens2, HoloLens1, Editor
    }
    
    public SupportedPlatform RuntimePlatform { get; 
        private set; 
    }

    public static void SetGlobals(bool showProfiler)
    {
        Debug.developerConsoleVisible = false;
        CoreServices.DiagnosticsSystem.ShowProfiler = showProfiler;
    }

    // Start is called before the first frame update
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            // DontDestroyOnLoad(gameObject);
        }
        else if (instance != this)
        {
            Debug.LogError("Multiple GlobalSettings instances");
            Destroy(gameObject);
            return;
        }
        
        SetGlobals(showProfiler);
        
        switch (Application.platform)
        {
            case UnityEngine.RuntimePlatform.Android:
                RuntimePlatform = SupportedPlatform.Android;
                break;
            case UnityEngine.RuntimePlatform.WSAPlayerARM:
                RuntimePlatform = SupportedPlatform.HoloLens2;
                break;
            case UnityEngine.RuntimePlatform.WSAPlayerX64:
            case UnityEngine.RuntimePlatform.WSAPlayerX86:
                RuntimePlatform = SupportedPlatform.HoloLens1;
                break;
            case UnityEngine.RuntimePlatform.LinuxEditor:
            case UnityEngine.RuntimePlatform.OSXEditor:
            case UnityEngine.RuntimePlatform.WindowsEditor:
                RuntimePlatform = SupportedPlatform.Editor;
                break;
            default:
                Debug.LogError($"Unsupported Platform: {Application.platform}");
                RuntimePlatform = SupportedPlatform.Unsupported;
                break;

        }
    }


    public void ResetScene()
    {
        PhotonNetwork.LoadLevel(SceneManager.GetActiveScene().name);

    }
    

    
}
