﻿using System;
using Photon.Pun;
using TMPro;
using UnityEngine;

public class NetworkGameSpaceObject : MonoBehaviourPun
{
	[SerializeField] private TextMeshPro nameTag;

	public static Color GetPlayerColor(int actorNumber) => Color.HSVToRGB(actorNumber * Mathf.PI % 1, 1, 1); 

	[PunRPC]
    void SetParent()
    {
	    gameObject.transform.SetParent(NetworkBrainManager.Instance.GameWorldSpace.transform);
    }
	
	[PunRPC]
    void SetColor()
    {
	    var m = new MaterialPropertyBlock();
	    var color = GetPlayerColor(photonView.Owner.ActorNumber); 
	    m.SetVector(Shader.PropertyToID("_Color"), color);
	    GetComponent<Renderer>().SetPropertyBlock(m);
    }

    [PunRPC]
    void SetText()
    {
	    nameTag.text = photonView.Owner.NickName;
    }
    
    [PunRPC]
    void SetActive(bool active)
    {
	    gameObject.SetActive(active);
    }

    [PunRPC]
    void SetNameTagLookAt()
    {
	    if (!photonView.IsMine)
		    nameTag.gameObject.AddComponent<LookAtCamera>();
    }
}
