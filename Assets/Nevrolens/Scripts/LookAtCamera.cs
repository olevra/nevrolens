﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCamera : MonoBehaviour
{
    private Transform mainCamera;

    public bool active = true;

    public bool lockY = true;

    public bool inverse = true;
    // Start is called before the first frame update
    
    void Start()
    {
        mainCamera = Camera.main.transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (!active)
            return;
        if (!lockY) 
            transform.LookAt(mainCamera.position);
        else 
        {
            var pos = mainCamera.position;
            pos.y = transform.position.y;
            transform.LookAt(pos);
        }
        
        if (inverse)
            transform.Rotate(Vector3.up, 180);
    }
}
