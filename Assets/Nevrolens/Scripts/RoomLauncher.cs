﻿using System.Linq;
using System.Text.RegularExpressions;
using Nevrolens.Helpers;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RoomLauncher : MonoBehaviourPunCallbacks
{
    private string gameVersion = "1";
   
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        GlobalSettings.SetGlobals(showProfiler: false);
        PhotonNetwork.AutomaticallySyncScene = true;
    }
   
    private void Start()
    {
	    PhotonNetwork.LocalPlayer.NickName = GenerateName();
        ConnectToServer();
    }
   
    private void ConnectToServer()
    {
        if (PhotonNetwork.IsConnected)
        {
            PhotonNetwork.JoinRandomRoom();
        }
        else
        {
            PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = "eu";
            PhotonNetwork.ConnectUsingSettings();
            PhotonNetwork.GameVersion = gameVersion;
        }
    }
   
    public override void OnConnectedToMaster()
    {
        Debug.Log("OnConnectedToMaster");
        PhotonNetwork.JoinRandomRoom();
    }
   
    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log("OnJoinRandomFailed. Creating new room");
        PhotonNetwork.CreateRoom("", new RoomOptions{ MaxPlayers = 10 });
    }

    [Tooltip("1 = system, 3 = regex for lols")]
    [SerializeField] private int namingMethod = 1;

    private string GenerateName()
    {
        var username = $"Noname_{(int) (Random.value * 100)}"; // backup name :/

        if (namingMethod == 1)
        {
            username = $"System_name_{(int) (Random.value * 100)}"; // backup name :/
            
            var usr = "Users"; 
            var path = Application.persistentDataPath.Split("\\/".ToCharArray()).ToList();

            if (path.Contains(usr))
            {
                var i = path.IndexOf(usr) + 1;
                if (path.Count > i)
                {
                    username = path[i].Split('@').First(); // The HL2imtelAR02 part in c:/Data/Users/HL2imtelAR02@aoutlook.com/AppData/Local/Packages/nevrolens_pzq3xp76mxafg/LocalState 
                }
            }
        }
        else if (namingMethod == 3) // same as 1 but with regex
        {
            var match = Regex.Match(Application.persistentDataPath, $".*?Users[\\/](?<username>.*?)[@\\/].*", RegexOptions.IgnoreCase);
            username = match.Groups["username"].Value;

            if (string.IsNullOrWhiteSpace(username))
            {
                namingMethod = 1;
                GenerateName();  // hopefully no infine recurrsion 
            }
        }

        return username;
    }
    public override void OnJoinedRoom()
    {
        Debug.Log("Joined a room");
        
        if (PhotonNetwork.IsMasterClient)
            PhotonNetwork.LoadLevel("NevrolensPhotonScene");
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        Debug.Log($"{newPlayer.NickName} joined the room");
    }

}
