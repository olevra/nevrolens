﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.MixedReality.Toolkit.UI;
using Microsoft.MixedReality.Toolkit.UI.BoundsControl;
using Microsoft.MixedReality.Toolkit.Utilities;
using Microsoft.MixedReality.Toolkit.Utilities.Solvers;
using Photon.Pun;
using UnityEngine;

public class BrainControlMenu : MonoBehaviour
{
    // [SerializeField] private GameObject manipulationMenu;
    // [SerializeField] private GameObject defaultMenu;
    [SerializeField] private GridObjectCollection menuContent;

    private NetworkBrainManager BrainManager => NetworkBrainManager.Instance;

    [SerializeField] private NetworkClippingSystem clippingSystem;

    [SerializeField] private BrainPartSelectionMenu brainPartSelectionMenu;

    [SerializeField] private BoundsControl brainBoundsControl;

    public Dictionary<string, Interactable> Buttons { get; private set; }

    // public Dictionary<string, HandMenuSlice> HandMenuSlices { get; private set; }

    public enum MenuState
    {
        Manipulation, Default
    }

    [SerializeField] private MenuState initialMenuState = MenuState.Manipulation;

    private MenuState currentMenuState;
    public MenuState CurrentMenuState
    {
        get => currentMenuState;
        set
        {
            brainBoundsControl.TargetBounds.enabled = value == MenuState.Manipulation;
            brainBoundsControl.enabled = value == MenuState.Manipulation;
            brainBoundsControl.GetComponent<ObjectManipulator>().enabled = value == MenuState.Manipulation;
            brainBoundsControl.Active = value == MenuState.Manipulation;
            currentMenuState = value;
        }
    }
    
    public enum ClippingState
    {
        Off, On
    }

    [SerializeField] private ClippingState initialClippingState;
    private ClippingState currentClippingState;
    public ClippingState CurrentClippingState
    {
        get => currentClippingState;
        set
        {
            // TODO: add option in menu for disabling collider while dissection
            BrainManager.brain.GetComponentsInChildren<BrainPart>()
                .Select(bp => bp.GetComponent<Collider>())
                .ToList()
                .ForEach(m => m.enabled = (value == ClippingState.Off));
            
            clippingSystem.gameObject.SetActive(value == ClippingState.On);
            currentClippingState = value;
        }
    }

    public enum FindState
    {
        Off, On
    }

    [SerializeField] private FindState initialFindState;
    private FindState currentFindState;

    public FindState CurrentFindState
    {
        get => currentFindState;
        set
        {
            if (brainPartSelectionMenu != null)
                brainPartSelectionMenu.gameObject.SetActive(value == FindState.On);
            currentFindState = value;
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        CurrentMenuState = initialMenuState;
        CurrentClippingState = initialClippingState;
        CurrentFindState = initialFindState;
        
        var runtimePlatform = GlobalSettings.Instance.RuntimePlatform;
        if (runtimePlatform == GlobalSettings.SupportedPlatform.HoloLens2)
        {
            var handConstraintPalmUp = GetComponent<HandConstraintPalmUp>();
            menuContent.gameObject.SetActive(false);
            // handConstraintPalmUp.OnFirstHandDetected.AddListener(() => menuContent.gameObject.SetActive(true));
            handConstraintPalmUp.OnHandActivate.AddListener(() => menuContent.gameObject.SetActive(true));
            handConstraintPalmUp.OnHandDeactivate.AddListener(() => menuContent.gameObject.SetActive(false));
            // handConstraintPalmUp.enabled = false;
        }

        
        SetButtons();

        // var adminButtonName = Buttons.Keys.First(b => b.Contains("Admin"));
        // if (adminButtonName != null)
        // {
        //     var adminButton = Buttons[adminButtonName];
        //     var collection = adminButton.GetComponentInParent<GridObjectCollection>();
        //     foreach (var receiver in adminButton.GetReceivers<InteractableOnToggleReceiver>())
        //         receiver.OnDeselect.Invoke();
        //     adminButton.gameObject.SetActive(BrainManager.Admin.IsAdmin);
        //     collection.UpdateCollection();
        // }
        
    }

    class NetworkButtonSynchronizer
    {
        
    }

    void Toggler(Interactable button, bool toggleSet, bool on)
    {
        if (button.ButtonMode != SelectionModes.Toggle)
            return;
       
        var setToggle = (!toggleSet && !button.IsToggled) || (toggleSet && on); 
        
        foreach (var receiver in button.GetReceivers<InteractableOnToggleReceiver>())
        {

            if (setToggle)
                receiver.OnSelect.Invoke();
            else
                receiver.OnDeselect.Invoke();

        }

        button.IsToggled = setToggle;
    }

    void Clicker(Interactable button)
    {
        foreach (var rec in button.GetReceivers<InteractableOnPressReceiver>())
        {
           rec.OnPress.Invoke(); 
        }

        // button.OnClick.Invoke();
    }

    void Presser(Interactable button, bool toggleSet = false, bool on = false)
    {
        if (button.ButtonMode == SelectionModes.Toggle)
            Toggler(button, toggleSet, on);
        else
            Clicker(button);
        
    }

    // void SetHandMenuSlices()
    // {
    //     HandMenuSlices = menuContent.GetComponentsInChildren<HandMenuSlice>().ToDictionary(
    //         s => s.name,
    //         s => s
    //     ); 
    // }

    void SetButtons()
    {
        Buttons = GetComponentsInChildren<PressableButtonHoloLens2>().ToDictionary(
            b => b.name,
            b => b.GetComponent<Interactable>()
            );
    }

    bool TryGetNewButton(string buttonName, out Interactable button, bool cache = true)
    {
        var foundButton = GetComponentsInChildren<PressableButtonHoloLens2>().First(b => b.name == buttonName);
        if (foundButton == null)
        {
            button = null;
            return false;
        }
        
        button = foundButton.GetComponent<Interactable>();

        if (cache)
            Buttons[buttonName] = button;
        
        return true;


    }


    [PunRPC]
    void OnClickRPC(string buttonName, bool on)
    {
        if (!Buttons.TryGetValue(buttonName, out var button))
            if (!TryGetNewButton(buttonName, out button))
                Debug.LogError($"No button with name {buttonName}");

        Presser(button, true, on);
        
    }

    public void OnClick_Network_Button(Interactable button)
    {
     
        // if (PhotonNetwork.IsConnected)
        //     PhotonView.Get(this).RPC("OnClickRPC", RpcTarget.OthersBuffered, button.name);
        var on = true;
        if (button.ButtonMode == SelectionModes.Toggle)
            on = button.IsToggled;
        if (PhotonNetwork.IsConnected)
            PhotonView.Get(this).RPC("OnClickRPC", RpcTarget.OthersBuffered, button.name, on);
    }

    // public void OnClick_Manipulation_Ok()
    // {
    //     CurrentMenuState = MenuState.Default;
    // }
    
    public void OnClick_Adjust(bool on)
    {
        CurrentMenuState = on ? MenuState.Manipulation : MenuState.Default;
    }

    public void OnClick_Dissect(bool on)
    {
        CurrentClippingState = on ? ClippingState.On : ClippingState.Off;
    }

    public void OnClick_Find(bool on)
    {
        CurrentFindState = on ? FindState.On : FindState.Off;

    }
}
