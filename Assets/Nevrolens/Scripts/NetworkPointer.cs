﻿using System;
using Photon.Pun;
using UnityEngine;

public class NetworkPointer : MonoBehaviourPun
{
	
	[PunRPC]
    void SetParent()
    {
	    gameObject.transform.SetParent(NetworkBrainManager.Instance.GameWorldSpace.transform);
    }

	[PunRPC]
    void SetColor()
    {
        
	    var m = new MaterialPropertyBlock();
	    var color = NetworkGameSpaceObject.GetPlayerColor(PhotonView.Get(this).Owner.ActorNumber); 
	    m.SetVector(Shader.PropertyToID("_Color"), color);
	    GetComponent<Renderer>().SetPropertyBlock(m);
    }

    [PunRPC]
    void SetActive(bool active)
    {
	    gameObject.SetActive(active);
    }

}