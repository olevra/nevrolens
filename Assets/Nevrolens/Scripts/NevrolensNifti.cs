﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using NiftiNET;
using UnityEngine;

namespace Nevrolens.Helpers
{
	public class NevrolensNifti
    {

		public static void JsonSerialize(SerializableNifti nifti, string path)
		{
			if (File.Exists(path))
				File.Delete(path);
			var json = JsonUtility.ToJson(nifti);
			// var json = JsonSerializer.Serialize(nifti);
			File.WriteAllText(path, json);
		}
		
		public static SerializableNifti JsonDeserialize(string jsonText)
		{
			// var jsonText = File.ReadAllText(path);
			return JsonUtility.FromJson<SerializableNifti>(jsonText);

		}
	    public static void SerializeNow<T>(T c, string path) {  
			// File f = new File("temp.dat");  
			// Stream s = f.Open(FileMode.Create);  
			if (File.Exists(path))
				File.Delete(path);
			Stream s = File.Create(path);
			var b = new BinaryFormatter
			{
				AssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple
			};
			b.Serialize(s, c);  
			s.Close();  
		}  
		public static T DeSerializeNow<T>(string path) {  
			Stream s = File.OpenRead(path);
			return DeSerializeNow<T>(s);
		}

		public static T DeSerializeNow<T>(Stream stream)
		{
			var b = new BinaryFormatter
			{
				AssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple
			};
			var c = (T) b.Deserialize(stream);  
			stream.Close();
			return c;
		}
    }
}
