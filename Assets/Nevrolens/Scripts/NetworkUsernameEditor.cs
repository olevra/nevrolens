﻿using System;
using Microsoft.MixedReality.Toolkit.Experimental.UI;
using Photon.Pun;
using UnityEngine;
using Random = UnityEngine.Random;

public class NetworkUsernameEditor : MonoBehaviourPun
{
    private TouchScreenKeyboard keyboard;

    [SerializeField] private MixedRealityKeyboard mixedRealityKeyboard;

    void Update()
    {
        if (keyboard != null && !keyboard.active)
        {
            SetUsername(keyboard.text);
            keyboard = null;
        }
    }

    private void SetUsername(string username)
    {
        if (string.IsNullOrWhiteSpace(username)) return;
        PhotonNetwork.LocalPlayer.NickName = username;
        Debug.Log($"New username {username} || {PhotonNetwork.LocalPlayer.NickName}");
    }

    public void OnCommitText()
    {
        SetUsername(mixedRealityKeyboard.Text);
    }

    public void OnClick_EditUsername()
    {
        #if UNITY_EDITOR
            PhotonNetwork.LocalPlayer.NickName = $"Test player {Random.Range(0, 100)}";
        #endif
        
        
        if (GlobalSettings.Instance.RuntimePlatform == GlobalSettings.SupportedPlatform.Android)
            keyboard = TouchScreenKeyboard.Open(PhotonNetwork.LocalPlayer.NickName, TouchScreenKeyboardType.Default);
        else
            mixedRealityKeyboard.ShowKeyboard();
        
    }
}
