﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(Microsoft.MixedReality.Toolkit.Utilities.ClippingPlane))]
public class NetworkClippingSystem : MonoBehaviour
{

    [SerializeField] public BrainClippingPlane clippingPlane;
    [SerializeField] public ClippingBall clippingBall;

    private void Awake()
    {
        clippingPlane.UseOnPreRender = true;
    }

    public void UpdateRenderers(IEnumerable<Renderer> renderers = null)
    {
        clippingPlane.UpdatePlane();
    }

    private void Start()
    {
        PopulateClippingPlaneRenderers(NetworkBrainManager.Instance.BrainParts.Select(b => b.BrainPartRenderer));
    }

    private void OnEnable()
    {
        UpdateRenderers();
}

    public void PopulateClippingPlaneRenderers(IEnumerable<Renderer> renderers, bool clearRenderers = true)
    {
        if (clearRenderers)
            clippingPlane.ClearRenderers();
        foreach (var r in renderers) 
            clippingPlane.AddRenderer(r);
        UpdateRenderers();
    }

}
