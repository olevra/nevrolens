using System;
using System.Collections.Generic;
using System.Linq;
using Nevrolens.Helpers;
using UnityEngine;

public class Clustering : MonoBehaviour
{
    private struct ClusterSettings
    {
        public float minColor, maxColor;

    }
    
    
    private Dictionary<string, List<BrainPart>> clusters;
    private Dictionary<string, ClusterSettings> clusterSetting = new Dictionary<string, ClusterSettings>();  

    [SerializeField] private TextAsset clusteringText;
    private NetworkBrainManager BrainManager => NetworkBrainManager.Instance;
    [SerializeField] private GameObject brainOutline;
    

    private int clusterIndex = 0;
    private bool showOutline = true;

    private bool ShowOutline
    {
        get => showOutline;
        set
        {
            brainOutline.SetActive(value);
            showOutline = value;
        }
    }

    private bool useClusterColor = true;
    private bool UseClusterColor
    {
        get => useClusterColor;
        set
        {
            useClusterColor = value;
            Onclick_Cluster(clusterActive);
        }
    }

    private bool clusterActive = false;

    // Start is called before the first frame update
    void Start()
    {
        clusters = ParseClustersAndMapToBrainPart(clusteringText.text);

        // EnableCluster(clusters.Keys.ToList()[clusterIndex]);
    }

    public void Onclick_Cluster(bool on)
    {
        // if (clusterIndex < 0 || clusterIndex >= clusters.Count) return;
        clusterActive = on;

        if (clusterActive)
        {
            var clusterName = clusters.Keys.ElementAt(clusterIndex);
            EnableCluster(clusterName);
        }
        else 
            DisableCluster();

    }

    public void Onclick_Next()
    {
        clusterIndex++;
        if (clusterIndex >= clusters.Count)
            clusterIndex = 0;
        Onclick_Cluster(clusterActive);
    }

    public void Onclick_Outline(bool on)
    {
        ShowOutline = on;
    }

    public void Onclick_Color(bool on)
    {
        UseClusterColor = on;

    }


    private void EnableCluster(string clusterName)
    {
        var clusterParts = clusters[clusterName];

        foreach (var brainPart in BrainManager.BrainParts)
        {
            var isInCluster = clusterParts.Contains(brainPart); 
            brainPart.gameObject.SetActive(isInCluster);
        }

        var settings = clusterSetting[clusterName];
        
        BrainManager.SetColors(clusterParts, UseClusterColor, settings.minColor, settings.maxColor);
        BrainManager.BrainTitle = clusterName;
        brainOutline.SetActive(showOutline);

    }

    private void DisableCluster()
    {
        
        foreach (var brainPart in BrainManager.BrainParts)
        {
            brainPart.gameObject.SetActive(true);
        }
        BrainManager.SetColors();
        BrainManager.ResetBrainTitle();
        brainOutline.SetActive(false);
        
    }
    

    private Dictionary<string, List<BrainPart>> ParseClustersAndMapToBrainPart(string clusteringText)
    {
        var clusters = new Dictionary<string, List<BrainPart>>();
        
        var lines = clusteringText.Split(Environment.NewLine.ToCharArray());
        var currentClusterName = string.Empty;
        List<BrainPart> currentClusterList = null; 
        foreach (var line in lines)
        {
            if (string.IsNullOrWhiteSpace(line) || line[0] == '!')
                continue;

            if (line[0] == '#')
            {
                if (!string.IsNullOrEmpty(currentClusterName))
                {
                    clusters[currentClusterName] = currentClusterList ?? new List<BrainPart>();
                    // currentClusterName = string.Empty;
                    // currentClusterList.Clear();
                    
                }
                    
                var clusterName = line.Substring(1).Trim();
                currentClusterName = clusterName;
                currentClusterList = new List<BrainPart>();
            }
            else if (line[0] == '@')
            {
                var colors = line.Substring(1).Trim().Split(' ');
                if (colors.Length == 2)
                { 
                    var min  = int.Parse(colors[0]);
                    var max  = int.Parse(colors[1]);
                    
                    var fmin = ((float) min).Map(0f, 360f, 0f, 1f); 
                    var fmax = ((float) max).Map(0f, 360f, 0f, 1f);

                    clusterSetting.TryGetValue(currentClusterName, out var settings);
                    settings.minColor = fmin;
                    settings.maxColor = fmax;
                    clusterSetting[currentClusterName] = settings;
                }
            }
            else
            {
                if (string.IsNullOrEmpty(currentClusterName))
                    throw new ArgumentException("Invalid clustering file");
                
                var brainPartName = line.Trim();
                
                if (!BrainManager.NamedBrainParts.TryGetValue(brainPartName, out var brainPart))
                    Debug.LogWarning($"{brainPartName} is not a correct name of a brain part");
                else
                    currentClusterList.Add(brainPart);
            }
        }
        
        if (!string.IsNullOrEmpty(currentClusterName) && !clusters.ContainsKey(currentClusterName))
            clusters[currentClusterName] = currentClusterList ?? new List<BrainPart>();
            

        return clusters;
    }

    // Update is called once per frame
    // void Update()
    // {
    //     if (currentClusterIndex != clusterIndex)
    //     {
    //         Debug.Log($"{currentClusterIndex} {clusterIndex}");
    //         if (clusterIndex >= 0 && clusterIndex < clusters.Keys.Count)
    //             EnableCluster(clusters.Keys.ToList()[clusterIndex]);
    //         else 
    //             DisableCluster();
    //         
    //         currentClusterIndex = clusterIndex;
    //     }
    //     
    //}
}
