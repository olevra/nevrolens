﻿using System;
using Photon.Pun;
using UnityEngine;

public abstract class NetworkSpawner : MonoBehaviour
{
	[SerializeField] protected NetworkGameSpaceObject networkObjectPrefab;

	protected Transform cameraTransform;
    private NetworkGameSpaceObject networkObject;
    private Transform networkObjectTransform;
    private PhotonView networkObjectPhotonView;

    protected abstract void InitializeNetworkObject();
    protected abstract void UpdateNetworkObject();

    private Vector3 ToNetworkLocalPosition(Vector3 position)
    {
        return NetworkBrainManager.Instance.GameWorldSpace.transform.InverseTransformPoint(position);
    }
    
    protected void RPC(string methodName, RpcTarget rpcTarget, params object[] parameters) => networkObjectPhotonView.RPC(methodName, rpcTarget, parameters);

    protected void InitialRPCs(params string[] methodNames)
    {
        foreach (var methodName in methodNames)
        {
		    RPC(methodName, RpcTarget.AllBuffered);
        }
    }

    protected void UpdateGameSpaceTransform(Vector3 position, Quaternion rotation)
    {
		networkObjectTransform.localPosition = ToNetworkLocalPosition(position);
		networkObjectTransform.rotation = rotation; 
    }
    
    private void Start()
    {
	    
        if (!PhotonNetwork.IsConnected)
        {
            enabled = false;
            return;
        }

        cameraTransform = Camera.main.transform;
        
        networkObject = PhotonNetwork.Instantiate(networkObjectPrefab.name, Vector3.zero, Quaternion.identity)
            .GetComponent<NetworkGameSpaceObject>();
        networkObjectPhotonView = PhotonView.Get(networkObject);
        networkObjectTransform = networkObject.transform;

        foreach (var r in networkObject.GetComponentsInChildren<Renderer>()) r.enabled = false;
        
        InitializeNetworkObject();

    }

    private void Update()
    {
        UpdateNetworkObject();
    }
}