﻿using System;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

namespace Nevrolens.Scripts.Experimental
{
    public struct NiftiLabel
    {
        public int index;
        public Color color;
        public float transparency;
        public bool visibility;
        public bool meshVisibility;
        public string description;

    }

    public class NiftiLabelParser
    {

        public static List<NiftiLabel> Parse(string niftiLabelText)
        {
            var labels = new List<NiftiLabel>(); 
            var lines = niftiLabelText.Split(Environment.NewLine.ToCharArray());

            foreach (var line in lines)
            {

                if (string.IsNullOrWhiteSpace(line) || line[0] == '#')
                    continue;
                
                var label = new NiftiLabel();
                var desc = line.Split('"')[1];
                var l = line.Trim().Split(" \t".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);


                label.index = int.Parse(l[0].Trim());
                var r = int.Parse(l[1].Trim());
                var g = int.Parse(l[2].Trim());
                var b = int.Parse(l[3].Trim());
                var a = float.Parse(l[4].Trim(), CultureInfo.InvariantCulture);  // If you're computer is "på nåsjk" it will not be able to parse a float with "punktum", therefore CultureInfo. 

                label.color = new Color(r / 256f, g / 256f, b / 256f, a);
                label.transparency = a;
                label.visibility = int.Parse(l[5].Trim()) == 1;
                label.meshVisibility = int.Parse(l[6].Trim()) == 1;
                label.description = desc;
                labels.Add(label);
            }

            return labels;

        }
        
    }
}
