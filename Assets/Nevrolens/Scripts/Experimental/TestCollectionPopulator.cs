﻿using System.Collections;
using System.Collections.Generic;
using Microsoft.MixedReality.Toolkit.UI;
using Microsoft.MixedReality.Toolkit.Utilities;
using UnityEngine;
using UnityEngine.UI;

public class TestCollectionPopulator : MonoBehaviour
{
    public int numOfButtons = 20;
    // Start is called before the first frame update
    void Start()
    {
        var gos = GetComponentsInChildren<ButtonConfigHelper>();


        var neededChildren = numOfButtons - gos.Length;
        if (neededChildren > 0)
        {
            while (neededChildren > 0)
            {
                var go = gos[0];
                var newGo = Instantiate(go, transform, true);
                neededChildren--;
            }
            
            GetComponent<GridObjectCollection>().UpdateCollection();
            gos = GetComponentsInChildren<ButtonConfigHelper>();
        }
        
        
        var i = 0;
        foreach (var go in gos)
        {

            go.MainLabelText = $"Knapp {i}";
            var j = i;
            go.GetComponent<Interactable>().OnClick.AddListener(() => OnClick("bolle " + j.ToString() ));

            i++;
        }
    }

    public void OnClick(string name)
    {
       Debug.Log(name); 
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
