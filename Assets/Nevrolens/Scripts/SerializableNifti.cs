﻿using System;


namespace NiftiNET
{
	[Serializable]
	public class SerializableNifti
	{
		public ushort[] Data;


		public int[] Dimensions;


		public SerializableNifti(ushort[] data, int[] dimensions)
		{
			 Data = data;
			 Dimensions = dimensions;
		}
	}
	
}

