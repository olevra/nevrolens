﻿using System;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.UI;
using UnityEngine;


[RequireComponent(typeof(ObjectManipulator))]
public class SnapInPlace : MonoBehaviour
{

    
    [Range(0.01f, 1f)]
    public float snapRadius = 0.15f;

    public bool snapHintSphereEnabled = true;
    public GameObject snapHintSphere;

    public bool snapHintEnableOnDistance = false;
    public float snapHintEnableDistanceTreshold = 0.01f;
    public Color activeColor = new Color(91, 253, 35);
    public Color inactiveColor = new Color(162, 229, 255);
    public bool playSnapSound = true;
    public bool moveToSnapEnabled = false;
    
    [Range(1f, 100f)]
    public float moveSpeed = 20f;
    
    private bool isMoving = false;
    private IMixedRealityPointer pointer;

    private Transform followTransform;
    private AudioSource snapInSoundSource;

    private Vector3 initialPosition;
    private Quaternion initialRotation;
    private Renderer hintSphereRenderer;
    private static readonly int RimColor = Shader.PropertyToID("_RimColor");

    private Vector3 GlobalPosition => followTransform.position + initialPosition;
    private Quaternion GlobalRotation => followTransform.rotation * initialRotation;


    private BrainPart brainPart;
    public Mesh brainPartMesh;
    private bool isInManipulation;

    private Vector3 Scale { get; set; }
    private Vector3 GlobalScale { get; set; }

    public bool IsSnapped
    {
        get => isSnapped;
        set
        {
            var niftiPlane = NetworkBrainManager.Instance.niftiPlane;
            if (niftiPlane.FillDynamic)
            {
                var col = value ? brainPart.label.color : NetworkBrainManager.Instance.Labels[0].color; 
                niftiPlane.SetLookupColor(brainPart.label.index, col);
            }
            isSnapped = value;
        }
    }

    private bool isSnapped;

    private void Awake()
    {
        initialPosition = transform.localPosition;
        initialRotation = transform.localRotation;
        Scale = transform.localScale;
        
        GlobalScale = transform.lossyScale;
        
        brainPart = GetComponent<BrainPart>();
        
        followTransform = transform.parent;
        snapInSoundSource = GetComponent<AudioSource>();
        
    }

    private void Start()
    {
        IsSnapped = true;
        GetComponent<ObjectManipulator>().OnManipulationStarted.AddListener(OnManipulationStarted);
        GetComponent<ObjectManipulator>().OnManipulationEnded.AddListener(OnManipulationEnded);
    }

    void OnEnable()
    {
        // var mesh = GetComponent<MeshFilter>().sharedMesh;
        brainPartMesh = brainPart.BrainPartMesh;
        IsSnapped = IsInInitialPosition(); 
        

        hintSphereRenderer = snapHintSphere.GetComponent<Renderer>();
        hintSphereRenderer.material.SetVector(RimColor, inactiveColor);
        snapHintSphere.SetActive(false);
        
    }

    private void OnDisable()
    {
        IsSnapped = false; 
    }


    public void Snap(bool playSound = true)
    {
        
        transform.position = GlobalPosition;
        transform.rotation = GlobalRotation;
        transform.localScale = Scale;
        IsSnapped = true;
        
        if (playSound & playSnapSound)
            PlaySnapSound();
    }

    float Distance(Vector3 position) => Vector3.Distance(GlobalPosition, position);

    private bool IsInSnapRange(Vector3 pointerPosition = default, bool checkPointer = false)
    {
        bool isPointerInside;

        if (!checkPointer)
            isPointerInside = false;
        else if (pointerPosition == default && pointer != null)
            isPointerInside = Distance(pointer.Result.Details.Point) < snapRadius;
        else if (pointerPosition == default)
            isPointerInside = false;
        else
            isPointerInside = Distance(pointerPosition) < snapRadius;

        return IsInInitialPosition(snapRadius) | isPointerInside;
    }

    void PlaySnapSound()
    {
        if (snapInSoundSource != null)
            snapInSoundSource.Play();
        else
            throw new Exception($"{gameObject.ToString()} has no detected AudioSource.");
    }

    private bool IsInInitialPosition(float thresholds = 0.001f) => Distance(transform.position) < thresholds;

    private void Update()
    {
        if (snapHintEnableOnDistance && isInManipulation && !IsInInitialPosition(snapHintEnableDistanceTreshold)) 
            SnapHintSetActive(true);
        
        if (isMoving)
        {
            transform.position = Vector3.MoveTowards(transform.position, GlobalPosition, moveSpeed * Time.deltaTime);
            
            if (IsInInitialPosition())
            {
                isMoving = false;
                Snap();
            }
        }
        else if (snapHintSphereEnabled)
        {
            if (!snapHintSphere.activeSelf)
                return;

            if (IsInSnapRange())
                hintSphereRenderer.material.SetVector(RimColor, activeColor);
            else
                hintSphereRenderer.material.SetVector(RimColor, inactiveColor);
        }
    }

    public void SnapHintSetActive(bool enable)
    {
        if (!snapHintSphereEnabled) return;

        if (enable)
        {
            snapHintSphere.GetComponent<MeshFilter>().sharedMesh = brainPartMesh;
            snapHintSphere.transform.position = GlobalPosition;
            snapHintSphere.transform.rotation = GlobalRotation;
            snapHintSphere.transform.localScale = GlobalScale;
            snapHintSphere.SetActive(true);
        }
        else
            snapHintSphere.SetActive(false);

    }
    
    private void OnManipulationStarted(ManipulationEventData eventData)
    {
        isInManipulation = true;
        pointer = eventData.Pointer;
        if (!snapHintEnableOnDistance)
            SnapHintSetActive(true);

    }

    private void OnManipulationEnded(ManipulationEventData eventData)
    {
        isInManipulation = false;
        pointer = null;

        SnapHintSetActive(false);

        if (IsInSnapRange(eventData.PointerCentroid))
        {
            if (moveToSnapEnabled)
                isMoving = true;
            else
                Snap();
        }
        else
            IsSnapped = false;

    }
}
