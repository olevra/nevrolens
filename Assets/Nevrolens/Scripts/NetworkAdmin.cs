﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ExitGames.Client.Photon.StructWrapping;
using Microsoft.MixedReality.Toolkit.UI;
using Microsoft.MixedReality.Toolkit.Utilities;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class NetworkAdmin : MonoBehaviourPun, IInRoomCallbacks
{
    public bool IsAdmin => PhotonNetwork.IsConnected && PhotonNetwork.IsMasterClient;

    [SerializeField] private Interactable adminButton;
    [SerializeField] private HandMenuSlice toolsHandMenuSlice;
    [SerializeField] private HandMenuSlice adminHandMenuSlice;
    
    [SerializeField] private NetworkPlayerSpawner networkPlayerSpawner;


    [SerializeField] private BrainControlMenu brainControlMenu;
    private NetworkBrainManager BrainManager => NetworkBrainManager.Instance;

    private void Start()
    {
		SetAdminControls();
    }
    
    private void OnEnable()
    {
        PhotonNetwork.AddCallbackTarget(this);
    }

    private void OnDisable()
    {
        PhotonNetwork.RemoveCallbackTarget(this);
    }
    

    void SetAdminControls()
    
    {
		var collection = adminButton.GetComponentInParent<GridObjectCollection>();
		adminButton.gameObject.SetActive(IsAdmin);
		adminHandMenuSlice.gameObject.SetActive(IsAdmin);
		adminHandMenuSlice.GetComponentInParent<GridObjectCollection>().UpdateCollection();
		
		collection.UpdateCollection();

		if (IsAdmin)
		{
			AllowInteraction(true);
			AllowTools(true);
		}
			

    }

    [PunRPC]
    void AllowTools(bool allow)
    {
	    // if (IsAdmin) return;
	    toolsHandMenuSlice.gameObject.SetActive(allow);
		toolsHandMenuSlice.GetComponentInParent<GridObjectCollection>().UpdateCollection();
    }
    
    [PunRPC]
    void AllowInteraction(bool allow)
    {
	    foreach (var brainPart in BrainManager.BrainParts)
	    {
		    // brainPart.GetComponent<PhotonView>().OwnershipTransfer = OwnershipOption.Fixed;
		    brainPart.GetComponent<ObjectManipulator>().enabled = allow;
	    }

	    BrainManager.clippingSystem.clippingBall.GetComponent<ObjectManipulator>().enabled = allow;
    }

    public void OnToggle_AllowTools(bool on)
    {
	   PhotonView.Get(this).RPC("AllowTools", RpcTarget.OthersBuffered, on);
    }

    public void OnToggle_AllowInteraction(bool on)
    {
	   PhotonView.Get(this).RPC("AllowInteraction", RpcTarget.OthersBuffered, on);
    }
    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnPlayerEnteredRoom(Player newPlayer)
    {
	    Debug.Log($"New player {newPlayer.NickName}");
		
    }

    public void OnPlayerLeftRoom(Player otherPlayer)
    {
	    Debug.Log($"Player left {otherPlayer.NickName}");
		
    }

    public void OnRoomPropertiesUpdate(Hashtable propertiesThatChanged)
    {
		
    }

    public void OnPlayerPropertiesUpdate(Player targetPlayer, Hashtable changedProps)
    {
	    Debug.Log($"Property changed {targetPlayer.NickName} {changedProps}");
	    
	    // if (changedProps.ContainsKey("Nickname")) //does not work
	    if (Equals(targetPlayer, PhotonNetwork.LocalPlayer))
		    NetworkBrainManager.Instance.onNickNameChanged.Invoke(); 
			// networkPlayerSpawner.OnNickNameChanged();
    }

    public void OnMasterClientSwitched(Player newMasterClient)
    {
		SetAdminControls();
    }
}
