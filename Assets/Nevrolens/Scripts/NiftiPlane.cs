﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nevrolens.Scripts.Experimental;
using UnityEngine;
using static UnityEngine.Matrix4x4;

public class NiftiPlane : MonoBehaviour
{
    [SerializeField] private Renderer[] planeRenderers;
    [SerializeField] private Transform brain;
    [SerializeField] private Transform sourceTransform;

    [SerializeField] public Transform offsetTransform;

    private static readonly int PlaneTransformMatrix = Shader.PropertyToID("_PlaneTransformMatrix");
    private static readonly int NiftiColorLookupTable = Shader.PropertyToID("_NiftiColorLookupTable");
    private static readonly int ColorMode = Shader.PropertyToID("_ColorMode");
    private static readonly int NiftiColorLookupTableLength = Shader.PropertyToID("_NiftiColorLookupTableLength");
    
    [NonSerialized] private Texture2D niftiColorLookupTable;
    private int niftiLookupTableLenght;
    
    private Vector3 initialPos;
    private Quaternion initialInverseRot;
    private Vector3 initialBrainPos;


    //Why do we use 4x4 Matrices in Computer Graphics? https://www.youtube.com/watch?v=Do_vEjd6gF0 
    private Matrix4x4 textureSpaceMatrix;
    private Matrix4x4 modelSpaceMatrix;
    private Matrix4x4 planeSpaceMatrix;
    private Matrix4x4 planeProjectionMatrix;
    
    private Vector3 modelToTextureTranslation;
    private Vector3 modelScale;
    private Quaternion textureToModelRotation;

    private bool lookupHasChanged;
    private bool fillDynamic;
    public bool FillDynamic
    {
        get => fillDynamic;
        set
        {
            fillDynamic = value;
            FillLookupTable();
        }
    }

    public void InitColorLookupTable()
    {
        niftiLookupTableLenght = NetworkBrainManager.Instance.Labels.Last().index;
        niftiColorLookupTable = new Texture2D(niftiLookupTableLenght, 1, TextureFormat.RGBA32, false)
        {
            filterMode = FilterMode.Point, 
            wrapMode = TextureWrapMode.Clamp
        };

        FillLookupTable();

        
        foreach (var r in planeRenderers)
        {
            r.material.SetFloat(NiftiColorLookupTableLength, niftiLookupTableLenght);
            r.material.SetFloat(ColorMode, 2);
            r.material.SetTexture(NiftiColorLookupTable, niftiColorLookupTable);
        }
    }


    public void SetLookupColor(int index, Color color, bool changeOnlyIfNeeded = true)
    {
        if (changeOnlyIfNeeded && niftiColorLookupTable.GetPixel(index, 0).Equals(color)) return;
        
        niftiColorLookupTable.SetPixel(index, 0, color);
        lookupHasChanged = true;
    }

    public void ApplyLookupTable(bool forceApply = false)
    {
        if (lookupHasChanged || forceApply)
            niftiColorLookupTable.Apply();
        lookupHasChanged = false;
    }

    private void FillLookupTable(bool apply = true)
    {
        if (FillDynamic)
        {
            var clear = Enumerable.Repeat(NetworkBrainManager.Instance.Labels[0].color, niftiLookupTableLenght).ToArray();
            niftiColorLookupTable.SetPixels(clear);
            foreach (var brainPart in NetworkBrainManager.Instance.BrainParts)
            {
                if (brainPart.SnapInPlace.IsSnapped)
                    SetLookupColor(brainPart.label.index, brainPart.label.color);
            }
        }
        else
        {
            foreach (var label in NetworkBrainManager.Instance.Labels)
                niftiColorLookupTable.SetPixel(label.index, 0, label.color);
        }
        
        if (apply)
            ApplyLookupTable(forceApply: true);
        
    }

    public void Onclick_DynamicNiftiPlane(bool on)
    {
        FillDynamic = on;
    }


    void Start()
    {
        initialPos = sourceTransform.position;
        initialInverseRot = Quaternion.Inverse(sourceTransform.rotation);
        initialBrainPos = brain.position;
        Quaternion.Inverse(brain.rotation);
        
        modelToTextureTranslation =  new Vector3(0.5f, 0.5f, 0.5f);     // Relation between model center (0, 0 ,0) and texture space center (0.5, 0.5, 0.5)
        textureToModelRotation = Quaternion.Euler(180, 0, 0);           // Rotation difference between Texture3D and the geometric model (this is probably a result of wrong parsing when I created the 3D texture from nifti-data)
        modelScale = new Vector3(1, 2, 1);                              // Relation between texture space and game/model space (in texture space all dimensions are of unit length, while the model is (1, 2, 1)                        

        textureSpaceMatrix = Translate(modelToTextureTranslation) *
                             Inverse(
                                 Rotate(textureToModelRotation) * Scale(modelScale)
                             ); 
        
        planeProjectionMatrix = Translate(
            (Vector3.left + Vector3.down) * 0.5f  // Relation between the origo point of the plane GameObject and origo of a 2D texture mapped onto it (left top corner) seems like one of the axis is wrong tho
        );
        
        InitColorLookupTable();

    }

    void Update()
    {
        // InefficientFindBrainPartColor_ItShouldProbablyBeDoneWithEventFromSnapInPlaceOrSomething();
        ApplyLookupTable();
        
        if ((offsetTransform && offsetTransform.hasChanged) || brain.hasChanged || sourceTransform.hasChanged)
        {
            var m = new MaterialPropertyBlock();
            m.SetMatrix(PlaneTransformMatrix, UpdateMatrix());
            foreach (var r in planeRenderers) r.SetPropertyBlock(m);
        }

        brain.hasChanged = false;
        sourceTransform.hasChanged = false;
        
        if (offsetTransform)
            offsetTransform.hasChanged = false;
    }

    Matrix4x4 UpdateMatrix()
    {
        modelSpaceMatrix = TRS(
            brain.position - initialBrainPos,
            brain.rotation, 
            brain.lossyScale
            );
        
        planeSpaceMatrix = TRS(
            sourceTransform.position - initialPos, 
            sourceTransform.rotation * initialInverseRot, 
            sourceTransform.lossyScale
            );
        
        
        var offsetMatrix = !offsetTransform ? identity : TRS(offsetTransform.position, offsetTransform.rotation, offsetTransform.lossyScale);
        
        return textureSpaceMatrix * offsetMatrix * Inverse(modelSpaceMatrix) * planeSpaceMatrix * planeProjectionMatrix;
    }
    
}
