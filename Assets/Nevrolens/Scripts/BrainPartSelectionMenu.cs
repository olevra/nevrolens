﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.UI;
using Microsoft.MixedReality.Toolkit.Utilities;
using UnityEngine;

public class BrainPartSelectionMenu : MonoBehaviour
{

    private GridObjectCollection gridObjectCollection;
    [SerializeField] private ScrollingObjectCollection selectionScrollList;
    private bool scrollListActive = false;


    private NetworkBrainManager BrainSystem => NetworkBrainManager.Instance;

    // Start is called before the first frame update
    void Start()
    {
        
        gridObjectCollection = selectionScrollList.GetComponentInChildren<GridObjectCollection>();
        // var l = new List<BrainPart>() {brainSystem.brainParts.First()};
        PopulateList(BrainSystem.BrainParts);
        // selectionScrollList.gameObject.SetActive(scrollListActive);

    }

    struct BrainPartSelectionEventData
    {
        public int index;
        public BrainPart brainPart;
        public ButtonConfigHelper button;

    }

    void PopulateList(IReadOnlyList<BrainPart> parts, bool sort = true)
    {
        if (sort)
            parts = parts.OrderBy(p => p.DisplayName).ToList();
        
        var buttons = gridObjectCollection.GetComponentsInChildren<ButtonConfigHelper>();

        var neededChildren = parts.Count - buttons.Length;
        if (neededChildren > 0)
        {
            while (neededChildren > 0)
            {
                Instantiate(buttons[0], gridObjectCollection.transform, true);
                neededChildren--;
            }
            
            gridObjectCollection.UpdateCollection();
            buttons = gridObjectCollection.GetComponentsInChildren<ButtonConfigHelper>();
        }
        
        for (var i = 0; i < parts.Count; i++)
        {
            var button = buttons[i];
            var part = parts[i];
            button.MainLabelText = part.DisplayName;
            
            var i1 = i;
            // var interactable = button.GetComponent<Interactable>();
            // interactable.GetReceiver<InteractableOnFocusReceiver>().OnFocusOn.AddListener(() => Debug.Log("kake!"));
            button.OnClick.AddListener(
                () => OnSelectionMenuClick(new BrainPartSelectionEventData {index = i1, brainPart = part, button = button}));
            
        }
     
    }

    public void OnFeatureMenuFindClick(bool active)
    {
        scrollListActive = active;
        selectionScrollList.gameObject.SetActive(scrollListActive);
    }
    
    void OnSelectionMenuClick(BrainPartSelectionEventData eventData)
    {
       Debug.Log(eventData.brainPart.DisplayName);
       BrainSystem.SelectedBrainPart = eventData.brainPart;
       // brainSystem.UpdateSelectedMenu(changePosition: false);
       eventData.button.GetComponent<Interactable>().IsToggled = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
