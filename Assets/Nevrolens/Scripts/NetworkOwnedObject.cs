﻿using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.MixedReality.Toolkit.Input;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(PhotonView))]        
public class NetworkOwnedObject : MonoBehaviourPun, IMixedRealityPointerHandler, IPunOwnershipCallbacks
{

    [SerializeField] private bool transfereOnManipulation = true;

    
    public class NetworkedOwnedObjectTransferOwnershipEvent: UnityEvent<bool> {}

    public NetworkedOwnedObjectTransferOwnershipEvent TransferOwnershipEvent;

    private void OnEnable()
    {
        PhotonNetwork.AddCallbackTarget(this);
    }

    private void OnDisable()
    {
        PhotonNetwork.RemoveCallbackTarget(this);
    }

    private void Start()
    {
        TransferOwnershipEvent = new NetworkedOwnedObjectTransferOwnershipEvent();
        
        if (transfereOnManipulation && PhotonNetwork.IsConnected)
            photonView.OwnershipTransfer = OwnershipOption.Takeover;
    }

    public void TransferOwnership()
    {
        if (PhotonNetwork.IsConnected && !photonView.IsMine) 
            photonView.TransferOwnership(PhotonNetwork.LocalPlayer);
    }

    public void OnPointerDown(MixedRealityPointerEventData eventData)
    {
    }

    public void OnPointerDragged(MixedRealityPointerEventData eventData)
    {
        if (transfereOnManipulation)
            TransferOwnership();
    }

    public void OnPointerUp(MixedRealityPointerEventData eventData)
    {
    }

    public void OnPointerClicked(MixedRealityPointerEventData eventData)
    {
    }

    public void OnOwnershipRequest(PhotonView targetView, Player requestingPlayer)
    {
    }

    public void OnOwnershipTransfered(PhotonView targetView, Player previousOwner)
    {
        TransferOwnershipEvent.Invoke(targetView.IsMine);
    }
}
