﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Nevrolens.Helpers;
using Random = UnityEngine.Random;

public class ChangeColor : MonoBehaviour
{

    [Range(0.0f, 2.9f)]
    public float brightnessTreshold = 1.0f;
    // Start is called before the first frame update
    public void Awake()
    {
        //GetComponent<ClippingPlane>().renderer
    }

    public static void SetColors(IEnumerable<BrainPart> brainParts, float min = 0f, float max = 1f, bool reverse = false)
    {
        var bps = brainParts.ToArray();
        
        for (var i = 0; i < bps.Length; i++)
        {
            var bp = bps[i];
            var color = NextColorHSV((float) i / bps.Length, min: min, max: max, reverse: reverse); 
            
            bp.SetColor(color);
        }
    }

    private Color NextColorRandom(float v)
    {
        var r = Random.value;
        var g = Random.value;
        var b = Random.value; 
        return new Color(r, g, b, 1.0f);
        
    }

    public static Color NextColorHSVDeg(float v, int min = 0, int max = 360, bool reverse = false)
    {
        v %= 360f;
        v = v.Map(0f, 360f, 0f, 1f); 
        var fmin = ((float) min).Map(0f, 360f, 0f, 1f); 
        var fmax = ((float) max).Map(0f, 360f, 0f, 1f);

        return NextColorHSV(v, fmin, fmax, reverse); 
    }

    public static Color NextColorHSV(float v, float min = 0.0f, float max = 1.0f, bool reverse = false)
    {
        if (max <= min)
            max += 1.0f;

        if (reverse)
            (max, min) = (min, max); //swap 
        
        var hue = v.Map(0.0f, 1.0f, min, max);
        
        if (hue > 1.0f)
            hue -= 1.0f;
        return Color.HSVToRGB(hue, 1, 1);
    }
    
    
    public static Color NextColorDet(float v, Color? weights = null)
    {

        var w = weights ?? Color.white;
        
        v *= 2 * Mathf.PI;
        // var brainParts = GetComponentsInChildren<BrainPart>();
        // var N  = (float) brainParts.Length;
        var r = (Mathf.Sin(v) + 1) / 2;
        var g = (Mathf.Cos(v) + 1) / 2;
        var b = (-Mathf.Sin(v) + 1) / 2;
        return new Color(r * w.r, g * w.g, b * w.b, w.a);

    }

}
